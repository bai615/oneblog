-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: 2014-06-28 11:25:47
-- 服务器版本： 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `oa`
--

-- --------------------------------------------------------

--
-- 表的结构 `onethink_addons`
--

CREATE TABLE `onethink_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='插件表' AUTO_INCREMENT=48 ;

--
-- 转存表中的数据 `onethink_addons`
--

INSERT INTO `onethink_addons` (`id`, `name`, `title`, `description`, `status`, `config`, `author`, `version`, `create_time`, `has_adminlist`) VALUES
(43, 'EditorForAdmin', '后台编辑器', '用于增强整站长文本的输入和显示', 1, '{"editor_type":"2","editor_wysiwyg":"2","editor_markdownpreview":"1","editor_height":"500px","editor_resize_type":"1"}', 'thinkphp', '0.2', 1402218609, 0),
(25, 'SocialComment', '通用社交化评论', '集成了各种社交化评论插件，轻松集成到系统中。', 1, '{"comment_type":"2","comment_uid_youyan":"90040","comment_short_name_duoshuo":"jaylabs","comment_form_pos_duoshuo":"buttom","comment_data_list_duoshuo":"10","comment_data_order_duoshuo":"asc"}', 'thinkphp', '0.1', 1391765001, 0),
(26, 'Ping', '文章发布ping插件', '用于发布文档后的主动ping，主动增加收录', 0, '{"site_name":"\\u535a\\u5ba2","site_url":"blog.cn","update_url":"Archive\\/@blog.cn","update_rss":"feed@blog.cn"}', 'yangweijie', '0.1', 1392009365, 0),
(27, 'ReturnTop', '返回顶部', '回到顶部美化，随机或指定显示，100款样式，每天一种换，天天都用新样式', 1, '{"random":"0","current":"2"}', 'thinkphp', '0.1', 1392009776, 0),
(40, 'JqQrcode', 'jQuery二维码', '用jQuery 生成站点内所需要的二维码', 1, '{"article_status":"1","width":"200","height":"200","typeNumber":"-1","correctLevel":"2","background":"#ffffff","foreground":"#000000"}', 'yangweijie', '0.1', 1401319840, 0),
(44, 'VideoWall', '视频墙', '用于记录网上看的视频和自己上传的视频', 1, 'null', 'yangweijie', '0.2', 1403221689, 0);

-- --------------------------------------------------------

--
-- 表的结构 `onethink_article`
--

CREATE TABLE `onethink_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `type` int(3) unsigned NOT NULL DEFAULT '1',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '标题',
  `cate_id` int(10) unsigned NOT NULL COMMENT '所属分类',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至时间',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '扩展统计字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '文章内容',
  `tags` text NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `onethink_article`
--

INSERT INTO `onethink_article` (`id`, `uid`, `type`, `name`, `title`, `cate_id`, `description`, `link_id`, `cover_id`, `deadline`, `view`, `comment`, `extend`, `create_time`, `update_time`, `status`, `sort`, `parse`, `content`, `tags`) VALUES
(1, 1, 1, 'hello', 'Hello World', 1, '', 0, 0, 0, 0, 0, 0, 1404888695, 1404888695, 1, 0, 0, '当你看到这篇文章时候，你已经安装成功你的博客了。\r\n简单介绍下本博客系统。\r\n本博客是基于Thinkphp3.2开发的一个可定制化高的、小巧、高效博客。\r\n本博客实现了一般博客的常用功能：\r\n1. 发布、编辑文章\r\n2. 社交化评论\r\n3. 文章搜索、rss订阅、分页\r\n4. 文章分类和标签\r\n5. 一套模板可以实现轻松换肤\r\n6. 集成了onethink的插件机制，除了插件后台模板（自定义的）那部分插件拿过来要微调UI ，其他插件只要安装好对应有钩子，即可调用。一次开发终身受益。\r\n7. 归档显示\r\n8. 后台ui使用bootstrap2，方便定制美化。\r\n9. 上传驱动化支持SAE、BAE、又拍云、七牛、FTP等上传方式。\r\n10. 单页插件和文章的实现。\r\n\r\n本博客是基于原先onethink版重构的，精简了很多不需要的东西，比如独立模型、复杂的菜单权限和滥用的第三方组件。然后修复了很多bug还有优化了查询。\r\n\r\n下面是更新列表：\r\n修复了前台分页状态错误\r\n后台菜单重写，保证插件扩展的后台列表每次都能看见不是仅在插件列表中出现\r\n后台ui类用bootstrap默认的类名  ，以后换色系方便\r\n优化了tag组件，之前一个tag占一行，太tm占地方了\r\n优化了后台操作后提示效果，可以自动收http://git.oschina.net/yangweijie/oneblog起或跳转\r\n用thinkphp的调试工具条挨个页面检查，修复warnning提示，提高效率\r\n调试工具z-index被主题样式层挡住的问题\r\n前台分类、归档、标签等数据进行了缓存，当他们后台更新时会自动更新缓存\r\n后台添加了清除缓存功能（模板和数据缓存）\r\n重新调整了前台模板结构（目录Common改为Widget）更加语义化；模板只有一套目录，用模板继承保持简单，让开发者只要了解一套html结构就可以通过样式发挥魔力，在限制的情况下创新才更有挑战性，不必担心别的皮肤不可控情况发生，设计师只关心结构，无需关心数据从哪来。\r\n删除了很多无效的css\r\n新增使用了jQrcode二维码插件，手机分享访问自己文章不再是难事\r\n重新设计了表结构，由31张表缩减到11张表\r\n后台资源除了bootstrap 等组件就1个css、前台除了主题里的一个css文件和grid 重置css就没了。\r\n\r\n\r\n如果想二次开发的同学，再像ot一样的安装后，阅读下面的文档，能快速行动了。\r\n安装 访问/Install/\r\n\r\n最新版，大家可以去osc上 pull下来。欢迎大家测试、反馈、提交', ''),
(2, 1, 1, 'dev_doc', 'OneBlog 二次开发简单说明文档', 1, '', 0, 0, 0, 0, 0, 0, 1404888791, 1404888791, 1, 0, 0, '首先，开发分主题开发和插件功能开发。\r\n\r\n在后台里主题列表中，可以看到默认了2个主题，并且可以启用和编辑。一个是typecho的默认主题，简洁和支持响应式，一个是网上看到的效果比较好的 移植的。\r\n\r\n那么怎么制作一个主题呢。\r\n首先明白主题是针对一个html布局 一组展示主题想要表现的样式和其资源文件的目录。\r\n\r\n这个目录位于 /Public/css/theme/下，然后 主题名就是目录名，不过后台显示的还是其下面的ini里配置描述。\r\n\r\n一个主题有5个组成theme.css、theme.ini、images（图片资源目录）、screenshot.png（预览截图）、index.js 组成。\r\n\r\n一个theme.ini 的内容是这样的：\r\n; 主题信息\r\ndesc = 这是 Typecho 0.9 系统的一套默认皮肤\r\npackage = default\r\nauthor = Joker\r\nversion = 1.2\r\nlink = http://typecho.org分别是描述，主题名，作者，版本和作者博客地址。\r\n\r\n所有主题的css在主题目录里只有一个，里面只需要定义常用的类名实现想要的显示效果。\r\n\r\n从上到下可用的是：\r\nbody \r\n#header\r\n#hearder .description\r\n#logo\r\n#nav-menu\r\n#search\r\n.post\r\n.post-title\r\n.post-meta\r\n#main 左侧\r\n#secondry 右侧\r\n.widget\r\n.widget-list\r\n.widget .tag\r\n#footer 底部\r\n.page分页\r\n\r\n主题里复写了boostrap的末默认效果\r\n在主题.css中重写。如果页面结构满足不了当前htnl结构就在。indxi,js,建议写插入的js在indx页面里加入“dom元素”然后写添加过滤的\r\n\r\n实际的模板在/Application/home/view/中 主要用到模板继承、包含文件（带参数include）、widgets（部件）。\r\n\r\n父模板是这样的：\r\n&lt;!DOCTYPE HTML&gt;\r\n&lt;!--[if lt IE 7]&gt;      &lt;html class=&quot;no-js lt-ie9 lt-ie8 lt-ie7&quot;&gt; &lt;![endif]--&gt;\r\n&lt;!--[if IE 7]&gt;         &lt;html class=&quot;no-js lt-ie9 lt-ie8&quot;&gt; &lt;![endif]--&gt;\r\n&lt;!--[if IE 8]&gt;         &lt;html class=&quot;no-js lt-ie9&quot;&gt; &lt;![endif]--&gt;\r\n&lt;!--[if gt IE 8]&gt;&lt;!--&gt;\r\n&lt;html class=&quot;no-js&quot;&gt;\r\n &lt;!--&lt;![endif]--&gt;\r\n&lt;head&gt;\r\n    &lt;meta charset=&quot;UTF-8&quot;&gt;\r\n    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=edge, chrome=1&quot;&gt;\r\n    &lt;meta name=&quot;renderer&quot; content=&quot;webkit&quot;&gt;\r\n    &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1, maximum-scale=1&quot;&gt;\r\n    &lt;title&gt;{:C(''WEB_SITE_TITLE'')}&lt;/title&gt;\r\n    &lt;!-- 使用url函数转换相关路径 --&gt;\r\n    &lt;link rel=&quot;stylesheet&quot; href=&quot;__CSS__/grid.css&quot;&gt;\r\n    &lt;link rel=&quot;stylesheet&quot; href=&quot;__CSS__/theme/__THEME__/theme.css&quot;&gt;\r\n    &lt;!--[if lt IE 9]&gt;\r\n    &lt;script type=&quot;text/javascript&quot; src=&quot;__STATIC__/jquery-1.10.2.min.js&quot;&gt;&lt;/script&gt;\r\n    &lt;![endif]--&gt;&lt;!--[if gte IE 9]&gt;&lt;!--&gt;\r\n    &lt;script type=&quot;text/javascript&quot; src=&quot;__STATIC__/jquery-2.0.3.min.js&quot;&gt;&lt;/script&gt;\r\n    &lt;block name=&quot;style&quot;&gt;&lt;/block&gt;\r\n    &lt;!--[if lt IE 9]&gt;\r\n    &lt;script src=&quot;__JS__/html5shiv.js&quot;&gt;&lt;/script&gt;\r\n    &lt;script src=&quot;__JS__/respond.js&quot;&gt;&lt;/script&gt;\r\n    &lt;![endif]--&gt;\r\n    &lt;!-- 通过自有函数输出HTML头部信息 --&gt;\r\n    &lt;meta name=&quot;description&quot; content=&quot;{:C(''WEB_SITE_DESCRIPTION'')}&quot; /&gt;\r\n    &lt;meta name=&quot;keywords&quot; content=&quot;{:C(''WEB_SITE_KEYWORD'')}&quot; /&gt;\r\n    &lt;meta name=&quot;generator&quot; content=&quot;onethink,{$Think.THINK_VERSION}&quot; /&gt;\r\n    &lt;meta name=&quot;template&quot; content=&quot;default&quot; /&gt;\r\n    &lt;!-- &lt;link rel=&quot;pingback&quot; href=&quot;http://localhost/typecho/index.php/action/xmlrpc&quot; /&gt;\r\n    &lt;link rel=&quot;EditURI&quot; type=&quot;application/rsd+xml&quot; title=&quot;RSD&quot; href=&quot;http://localhost/typecho/index.php/action/xmlrpc?rsd&quot; /&gt;\r\n    &lt;link rel=&quot;wlwmanifest&quot; type=&quot;application/wlwmanifest+xml&quot; href=&quot;http://localhost/typecho/index.php/action/xmlrpc?wlw&quot; /&gt; --&gt;\r\n    &lt;link rel=&quot;alternate&quot; type=&quot;application/rss+xml&quot; title=&quot;RSS 2.0&quot; href=&quot;{:U(''/feed'')}&quot; /&gt;\r\n    &lt;link rel=&quot;alternate&quot; type=&quot;application/rdf+xml&quot; title=&quot;RSS 1.0&quot; href=&quot;{:U(''/feed/rss'')}&quot; /&gt;\r\n    &lt;link rel=&quot;alternate&quot; type=&quot;application/atom+xml&quot; title=&quot;ATOM 1.0&quot; href=&quot;{:U(''/feed/atom'')}&quot; /&gt;\r\n    {:hook(''pageHeader'')}\r\n&lt;/head&gt;\r\n&lt;body&gt;\r\n    &lt;!--[if lt IE 8]&gt;\r\n    &lt;div class=&quot;browsehappy&quot;&gt;当前网页 &lt;strong&gt;不支持&lt;/strong&gt; 你正在使用的浏览器. 为了正常的访问, 请 &lt;a href=&quot;http://browsehappy.com/&quot;&gt;升级你的浏览器&lt;/a&gt;.&lt;/div&gt;\r\n    &lt;![endif]--&gt;\r\n    &lt;block name=&quot;header&quot;&gt;\r\n    &lt;header id=&quot;header&quot; class=&quot;clearfix&quot;&gt;\r\n        &lt;div class=&quot;container&quot;&gt;\r\n            &lt;div class=&quot;colgroup&quot;&gt;\r\n                &lt;div class=&quot;site-name col-mb-12 col-9&quot;&gt;\r\n                    &lt;a id=&quot;logo&quot; href=&quot;{:U(''/'')}&quot;&gt;\r\n                        {:C(''WEB_SITE_NAME'')}\r\n                    &lt;/a&gt;\r\n                    &lt;p class=&quot;description&quot;&gt;{:C(''WEB_SITE_TITLE'')}&lt;/p&gt;\r\n                &lt;/div&gt;\r\n                &lt;div class=&quot;site-search col-3 kit-hidden-tb&quot;&gt;\r\n                    &lt;form id=&quot;search&quot; method=&quot;post&quot; action=&quot;/&quot; role=&quot;search&quot;&gt;\r\n                        &lt;label for=&quot;kw&quot; class=&quot;sr-only&quot;&gt;搜索关键字&lt;/label&gt;\r\n                        &lt;input type=&quot;text&quot; name=&quot;kw&quot; class=&quot;text&quot; placeholder=&quot;输入关键字搜索&quot; /&gt;\r\n                        &lt;button type=&quot;submit&quot; class=&quot;submit&quot;&gt;搜索&lt;/button&gt;\r\n                    &lt;/form&gt;\r\n                &lt;/div&gt;\r\n                &lt;div class=&quot;col-mb-12 navbar&quot;&gt;\r\n                    &lt;nav id=&quot;nav-menu&quot; class=&quot;clearfix&quot; role=&quot;navigation&quot;&gt;\r\n                        &lt;a class=&quot;current&quot; href=&quot;{:U(''/'')}&quot;&gt;首页&lt;/a&gt;\r\n                        {:W(''Common/single'')}\r\n                    &lt;/nav&gt;\r\n                &lt;/div&gt;\r\n            &lt;/div&gt;&lt;!-- end .colgroup --&gt;\r\n        &lt;/div&gt;\r\n    &lt;/header&gt;&lt;!-- end #header --&gt;\r\n    &lt;/block&gt;\r\n    &lt;div id=&quot;body&quot;&gt;\r\n        &lt;div class=&quot;container&quot;&gt;\r\n            &lt;div class=&quot;colgroup&quot;&gt;\r\n                &lt;div class=&quot;col-mb-12 col-8&quot; id=&quot;main&quot; role=&quot;main&quot;&gt;\r\n                    &lt;block name=&quot;main&quot;&gt;\r\n                    &lt;include file=&quot;Index:list&quot; /&gt;\r\n                    &lt;/block&gt;\r\n                &lt;/div&gt;&lt;!-- end #main--&gt;\r\n                &lt;block name=&quot;sidebar&quot;&gt;\r\n                    &lt;div class=&quot;col-mb-12 col-offset-1 col-3 kit-hidden-tb&quot; id=&quot;secondary&quot; role=&quot;complementary&quot;&gt;\r\n                        &lt;section class=&quot;widget&quot;&gt;\r\n                            &lt;h3 class=&quot;widget-title&quot;&gt;最新文章&lt;/h3&gt;\r\n                            &lt;ul class=&quot;widget-list&quot;&gt;\r\n                                {:W(''Common/new_article'')}\r\n                            &lt;/ul&gt;\r\n                        &lt;/section&gt;\r\n                        {:hook(''sidebar'')}\r\n                        &lt;section class=&quot;widget&quot;&gt;\r\n                            &lt;h3 class=&quot;widget-title&quot;&gt;分类&lt;/h3&gt;\r\n                            &lt;ul class=&quot;widget-list&quot;&gt;\r\n                               {:W(''Common/cates'')}\r\n                            &lt;/ul&gt;\r\n                        &lt;/section&gt;\r\n                        &lt;section class=&quot;widget&quot;&gt;\r\n                            &lt;h3 class=&quot;widget-title&quot;&gt;标签&lt;/h3&gt;\r\n                            {:W(''Common/tags'')}\r\n                        &lt;/section&gt;\r\n                        {:W(''Common/archive'')}\r\n                        &lt;section class=&quot;widget&quot;&gt;\r\n                            &lt;h3 class=&quot;widget-title&quot;&gt;功能&lt;/h3&gt;\r\n                            &lt;ul class=&quot;widget-list&quot;&gt;\r\n                            &lt;if condition=&quot;is_login()&quot;&gt;\r\n                                &lt;li class=&quot;last&quot;&gt;&lt;a href=&quot;{:U(''/Admin/System/login'')}&quot;&gt;进入后台 (admin)&lt;/a&gt;&lt;/li&gt;\r\n                                &lt;li&gt;&lt;a class=&quot;exit&quot; href=&quot;{:U(''/Admin/Public/logout'')}&quot;&gt;登出&lt;/a&gt;&lt;/li&gt;\r\n                            &lt;else /&gt;\r\n                                &lt;li&gt;&lt;a href=&quot;{:U(''/Admin/System/login'')}&quot;&gt;登录&lt;/a&gt;&lt;/li&gt;\r\n                            &lt;/if&gt;\r\n                                &lt;li&gt;&lt;a href=&quot;/feed&quot;&gt;文章 RSS&lt;/a&gt;&lt;/li&gt;\r\n                            &lt;/ul&gt;\r\n                        &lt;/section&gt;\r\n                    &lt;/div&gt;&lt;!-- end #sidebar --&gt;\r\n                &lt;/block&gt;\r\n            &lt;div class=&quot;clearfix&quot;&gt;&lt;/div&gt;\r\n            &lt;/div&gt;&lt;!-- end .colgroup --&gt;\r\n        &lt;/div&gt;\r\n    &lt;/div&gt;&lt;!-- end #body --&gt;\r\n    &lt;footer id=&quot;footer&quot; role=&quot;contentinfo&quot;&gt;\r\n        &amp;copy; 2014 &lt;a href=&quot;{:U(''/'')}&quot;&gt;{:C(''WEB_SITE_NAME'')}&lt;/a&gt;.\r\n        由 &lt;a href=&quot;http://www.thinkphp.cn&quot;&gt;thinkphp&lt;/a&gt; 强力驱动.\r\n        &lt;php&gt;$WEB_SITE_ICP = C(''WEB_SITE_ICP'');&lt;/php&gt;\r\n        &lt;notempty name=&quot;WEB_SITE_ICP&quot;&gt;{$WEB_SITE_ICP}&lt;/notempty&gt;\r\n    &lt;/footer&gt;&lt;!-- end #footer --&gt;\r\n    &lt;script type=&quot;text/javascript&quot;&gt;\r\n        var url = window.location.pathname + window.location.search;\r\n        url = url.replace(/(\\/(p)\\/\\d+)|(&amp;p=\\d+)|(\\/(id)\\/\\d+)|(&amp;id=\\d+)|(\\/(group)\\/\\d+)|(&amp;group=\\d+)/, &quot;&quot;);\r\n        var no_pic = ''__PUBLIC__/images/no-cover.png'';\r\n        (function(){\r\n            var ThinkPHP = window.Think = {\r\n                &quot;ROOT&quot;   : &quot;__ROOT__&quot;, //当前网站地址\r\n                &quot;APP&quot;    : &quot;__APP__&quot;, //当前项目地址\r\n                &quot;PUBLIC&quot; : &quot;__PUBLIC__&quot;, //项目公共目录地址\r\n                &quot;DEEP&quot;   : &quot;{:C(''URL_PATHINFO_DEPR'')}&quot;, //PATHINFO分割符\r\n                &quot;MODEL&quot;  : [&quot;{:C(''URL_MODEL'')}&quot;, &quot;{:C(''URL_CASE_INSENSITIVE'')}&quot;, &quot;{:C(''URL_HTML_SUFFIX'')}&quot;],\r\n                &quot;VAR&quot;    : [&quot;{:C(''VAR_MODULE'')}&quot;, &quot;{:C(''VAR_CONTROLLER'')}&quot;, &quot;{:C(''VAR_ACTION'')}&quot;]\r\n            }\r\n        })();\r\n        $(function(){\r\n             //单页高亮\r\n            $(''#nav-menu a'').removeClass(''current'');\r\n            $(''#nav-menu a[href=&quot;''+url+''&quot;]'').addClass(''current'');\r\n            $(''#search'').submit(function(){\r\n                var url = ''/search/''+ $(''#search input[name=&quot;kw&quot;]'').val();\r\n                location.href = url;\r\n                return false;\r\n            });\r\n        })\r\n    &lt;/script&gt;\r\n    &lt;script type=&quot;text/javascript&quot; src=&quot;__CSS__/theme/__THEME__/theme.js&quot;&gt;&lt;/script&gt;\r\n    {:hook(''pageFooter'')}\r\n    &lt;/body&gt;\r\n&lt;/html&gt;整个页面分上中下三部分。上是头部，有站点名称、站点标题和title 还有搜索框。中分左侧的文章列表（带分页）和右侧的侧边栏。\r\n下部就是版权等信息了。\r\n\r\n右侧有分类、归档、标签还有后台登陆等信息。\r\n布局如下：\r\n\r\n\r\n右侧的最新文章和分类级归档、功能结构都是一样的。除了tag样式要小调。\r\n最好的方法是新建一个空的theme.css放到一个主题目录里，启用，对比要仿的站，不用一个多小时，会点css的都能弄好一套皮肤。\r\n下面看我套好的皮肤：\r\n\r\n\r\n\r\n怎么样，立马从小清新变贵妇人了吧，嘎嘎。\r\n\r\n然后插件开发。\r\n\r\n首先开发方式和onethink官方的一样，不会的看文档, 和视频。\r\n然后本博客添加了一个特殊的钩子，single，这个钩子定位是钩子的功能就是一个独立的单页面应用。\r\n\r\n之前的ot版本由于没设计好，每次使用必须新建一个单页类型的文章和以文章标识同名的模板。现在不需要建模板，当单页文章有内容时，输出内容，没内容就调用标识名的钩子。因此标识名一定要写对插件的英文名（区分大小写）。因为是同一个Single钩子，所以插件名是当钩子方法的参数传递过去的。\r\n\r\n插件钩子实现必须加上判断如下：\r\n //实现的single钩子方法\r\n    public function single($param) {\r\n        if ($param[''name''] == ''VideoWall'') {\r\n            include_once ONETHINK_ADDON_PATH . ''VideoWall/function.php'';\r\n            $model = D(''Addons://VideoWall/Video'');\r\n            $list = $model-&gt;order(''create_time desc, update_time desc'')-&gt;select();\r\n            $months = $this-&gt;_dates($list);\r\n            $this-&gt;assign(''months'', $months);\r\n            $list = $this-&gt;_createObj($list);\r\n            // $config = get_addon_config(''VideoWall'');\r\n            $this-&gt;assign(''is_login'', is_login());\r\n            // $this-&gt;assign(''config'', $config);\r\n            $this-&gt;assign(''list'', $list);\r\n            trace($model-&gt;getField(''id,title,desp,cover,video_id,video_url,width,height,auto,preload,circle,create_time,update_time''));\r\n            $this-&gt;assign(''all_videos'', json_encode($model-&gt;getField(''id,title,desp,cover,video_id,video_url,width,height,auto,preload,circle,create_time,update_time'')));\r\n            $this-&gt;display(''single'');\r\n        }\r\n    }为了能让大家知道如何制作单页插件，我安装过后默认给大家装了一个视频墙的单页插件，并建好单页文章。\r\nPS：单页文章显示在首页右边，不计入常规文章统计和显示中。\r\n\r\n效果如下：\r\n怎么样，酷吧？\r\n\r\n侧边栏还预留了一个sidebar 钩子，目前的最新评论，之前用的本地数据库，没法通知对方。后来干脆用社交化的，如多说，顺便修复了下多说配置解析错误。\r\n加了一个侧边栏最新几条的钩子实现。上面默认皮肤的图有显示。文章详情页有用此评论，然后自己到多说后台可以加样式。绑定短域名。可以实现评论头像翻转效果，如图：\r\n\r\n\r\n一般人我不告诉他。\r\n\r\n    剩下的自己摸索吧。。祝你成功折腾出自己心中想要的博客。', '二次开发');

-- --------------------------------------------------------

--
-- 表的结构 `onethink_bookshell`
--

CREATE TABLE `onethink_bookshell` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` char(80) NOT NULL COMMENT '标题',
  `author` char(20) NOT NULL COMMENT '作者',
  `description` char(140) NOT NULL COMMENT '描述',
  `link_id` int(10) unsigned NOT NULL COMMENT '外链id',
  `cover_id` int(10) NOT NULL COMMENT '封面',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_cate`
--

CREATE TABLE `onethink_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `name` varchar(100) DEFAULT NULL COMMENT '英文标识',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `utime` int(11) unsigned DEFAULT NULL,
  `ctime` int(11) unsigned DEFAULT NULL,
  `pid` int(11) unsigned DEFAULT '0',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '数据状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `onethink_cate`
--

INSERT INTO `onethink_cate` (`id`, `title`, `name`, `content`, `utime`, `ctime`, `pid`, `sort`, `status`) VALUES
(1, '默认分类', 'blog', '', 1403456049, 1379474947, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `onethink_config`
--

CREATE TABLE `onethink_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` longtext NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=55 ;

--
-- 转存表中的数据 `onethink_config`
--

INSERT INTO `onethink_config` (`id`, `name`, `type`, `title`, `group`, `extra`, `remark`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES
(1, 'WEB_SITE_TITLE', 1, '网站标题', 1, '', '网站标题前台显示标题', 1378898976, 1379235274, 1, '基于thinkphp3.2的轻型博客', 8),
(2, 'WEB_SITE_DESCRIPTION', 2, '网站描述', 1, '', '网站搜索引擎描述', 1378898976, 1379235841, 1, '每个tper 新手该学习使用的博客，\r\n集成onethink里插件扩展机制的博客，\r\n可以换肤的博客，\r\n前台模板代码合理的博客。\r\n', 9),
(3, 'WEB_SITE_KEYWORD', 2, '网站关键字', 1, '', '网站搜索引擎关键字', 1378898976, 1381390100, 1, 'ThinkPHP,OneThink,OneBlog,Bootstrap2', 10),
(4, 'WEB_SITE_CLOSE', 4, '关闭站点', 1, '0:关闭,1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', 1378898976, 1379235296, 1, '1', 14),
(9, 'CONFIG_TYPE_LIST', 3, '配置类型列表', 4, '', '主要用于数据解析和页面表单的生成', 1378898976, 1379235348, 1, '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举', 26),
(10, 'WEB_SITE_ICP', 1, '网站备案号', 1, '', '设置在网站底部显示的备案号，如“沪ICP备12007941号-2', 1378900335, 1379235859, 1, '沪ICP备12007941号-2', 27),
(13, 'COLOR_STYLE', 4, '后台色系', 1, 'typecho_color:仿typecho', '后台颜色风格', 1379122533, 1392212877, 1, 'typecho_color', 28),
(20, 'CONFIG_GROUP_LIST', 3, '配置分组', 4, '', '配置分组', 1379228036, 1403913187, 1, '1:基本\r\n4:站点框架\r\n5:个人设置', 22),
(21, 'HOOKS_TYPE', 3, '钩子的类型', 4, '', '类型 1-用于扩展显示内容，2-用于扩展业务处理', 1379313397, 1379313407, 1, '1:视图\r\n2:控制器', 24),
(54, 'WEB_SITE_NAME', 1, '站点名称', 1, '', '用于底部或者介绍站点名称的地方', 1403913312, 1403913312, 1, 'OneBlog', 0),
(25, 'LIST_ROWS', 0, '后台每页记录数', 5, '', '后台数据每页显示记录数', 1379503896, 1392017862, 1, '10', 30),
(27, 'CODEMIRROR_THEME', 4, '预览插件的CodeMirror主题', 1, '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '详情见CodeMirror官网', 1379814385, 1390906341, 1, 'ambiance', 19),
(30, 'DATA_BACKUP_COMPRESS', 4, '数据库备份文件是否启用压缩', 4, '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持gzopen,gzwrite函数', 1381713345, 1381729544, 1, '1', 23),
(33, 'ALLOW_VISIT', 3, '不受限控制器方法', 4, '', '', 1386644047, 1403913198, 1, '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', 25),
(34, 'DENY_VISIT', 3, '超管专限控制器方法', 4, '', '仅超级管理员可访问的控制器方法', 1386644141, 1403913177, 1, '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', 13),
(36, 'ADMIN_ALLOW_IP', 2, '后台允许访问IP', 4, '', '多个用逗号分隔，如果不配置表示不限制IP访问', 1387165454, 1387165553, 1, '', 35),
(37, 'SHOW_PAGE_TRACE', 4, '是否显示页面Trace', 4, '0:关闭\r\n1:开启', '是否显示页面Trace信息', 1387165685, 1390274668, 1, '1', 33),
(41, 'FRONT_THEME', 0, '前台主题', 5, '', '前台主题配置，覆盖系统文件', 1392108077, 1403911179, 1, 'red', 2),
(40, 'FEEDFULLTEXT', 0, '聚合全文输出', 1, '0:仅输出摘要,1:全文输出', '如果你不希望在聚合中输出文章全文,请使用仅输出摘要选项.\r\n摘要的文字来自description字段，此字段无内容就截取前200个字符.', 1391997436, 1403911161, 1, '1', 1),
(42, 'PICTURE_UPLOAD_DRIVER', 4, '图片上传驱动类型', 4, 'Bcs:Bae-云环境\r\nSae:Sae-Storage\r\nLocal:Local-本地\r\nFtp:Ftp空间\r\nUpyun:有拍云', '需要配置相应的UPLOAD_{driver}_CONFIG 配置 放可使用，不然默认Local本地', 1393073505, 1393073505, 1, 'Local', 3),
(43, 'UPLOAD_BCS_CONFIG', 3, 'Bae上传配置', 4, '', '', 1393073559, 1393073559, 1, 'AccessKey:3321f2709bffb9b7af32982b1bb3179f\r\nSecretKey:67485cd6f033ffaa0c4872c9936f8207\r\nbucket:jaylab\r\nrename:0', 4),
(44, 'UPLOAD_SAE_CONFIG', 3, 'Sae上传配置', 4, '', 'sae Domain 配了 用domain ，没配用上传方法的第一个目录', 1393073998, 1393073998, 1, 'domain:123', 5),
(45, 'UPLOAD_QINIU_CONFIG', 3, '七牛上传配置', 4, '', '', 1393074989, 1393074989, 1, 'accessKey:ODsglZwwjRJNZHAu7vtcEf-zgIxdQAY-QqVrZD\r\nsecrectKey:Z9-RahGtXhKeTUYy9WCnLbQ98ZuZ_paiaoBjByKv\r\nbucket:onethinktest\r\ndomain:onethinktest.u.qiniudn.com\r\ntimeout:3600', 6),
(46, 'VERSION', 1, '站点版本', 4, '', '当前博客的版本', 1397351001, 1397956042, 1, '1.00beta', 7);

-- --------------------------------------------------------

--
-- 表的结构 `onethink_file`
--

CREATE TABLE `onethink_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `url` varchar(255) DEFAULT '' COMMENT '远程链接',
  `create_time` int(10) unsigned NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件表' AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_hooks`
--

CREATE TABLE `onethink_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `搜索索引` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `onethink_hooks`
--

INSERT INTO `onethink_hooks` (`id`, `name`, `description`, `type`, `update_time`, `addons`) VALUES
(1, 'pageHeader', '页面header钩子，一般用于加载插件CSS文件和代码', 1, 0, ''),
(2, 'pageFooter', '页面footer钩子，一般用于加载插件JS文件和JS代码', 1, 0, 'ReturnTop'),
(3, 'documentEditForm', '添加编辑表单的 扩展内容钩子', 1, 0, ''),
(4, 'documentDetailAfter', '文档末尾显示', 1, 1403317288, 'JqQrcode,SocialComment'),
(5, 'documentDetailBefore', '页面内容前显示用钩子', 1, 0, ''),
(6, 'documentSaveComplete', '保存文档数据后的扩展钩子', 2, 0, 'Ping'),
(7, 'documentEditFormContent', '添加编辑表单的内容显示钩子', 1, 0, ''),
(8, 'adminArticleEdit', '后台内容编辑页编辑器', 1, 1402216489, 'EditorForAdmin'),
(13, 'AdminIndex', '首页小格子个性化显示', 1, 1392170084, 'SystemInfo'),
(14, 'topicComment', '评论提交方式扩展钩子。', 1, 1380163518, ''),
(16, 'app_begin', '应用开始', 2, 1384481614, ''),
(17, 'sidebar', '前台首页侧边栏', 1, 1391602065, 'SocialComment'),
(18, 'single', '单页面专门用的钩子', 1, 1392448821, 'VideoWall,MovieLog,Bookshell,Timeline');

-- --------------------------------------------------------

--
-- 表的结构 `onethink_movie`
--

CREATE TABLE `onethink_movie` (
  `id` bigint(255) unsigned NOT NULL COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '中文名 ',
  `original_title` varchar(255) DEFAULT NULL COMMENT '原文名',
  `alt` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '条目URL',
  `images` text COMMENT '图片',
  `rating` text COMMENT '评分',
  `year` varchar(255) DEFAULT NULL COMMENT '年代',
  `subtype` varchar(255) DEFAULT 'movie' COMMENT '条目分类，movie或者tv',
  `ctime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  `is_published` tinyint(4) unsigned DEFAULT '0' COMMENT '是否发布人人 1-是 0-不是',
  `summary` text COMMENT '简介',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_picture`
--

CREATE TABLE `onethink_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_tags`
--

CREATE TABLE `onethink_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `count` int(10) unsigned DEFAULT '0' COMMENT '数量',
  `order` int(10) unsigned DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `onethink_tags`
--

INSERT INTO `onethink_tags` (`id`, `title`, `description`, `count`, `order`) VALUES
(1, '文档', NULL, 2, 0);

-- --------------------------------------------------------

--
-- 表的结构 `onethink_timeline`
--

CREATE TABLE `onethink_timeline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '标题',
  `startDate` varchar(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `endDate` varchar(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '媒体图片',
  `author` varchar(40) NOT NULL DEFAULT 'Jay' COMMENT '媒体作者',
  `media_title` char(40) NOT NULL DEFAULT '' COMMENT '媒体标题',
  `text` text COMMENT '事件内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_url`
--

CREATE TABLE `onethink_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '链接唯一标识',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短网址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='链接表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `onethink_user`
--

CREATE TABLE `onethink_user` (
  `id` int(11) NOT NULL,
  `name` longtext CHARACTER SET latin1 NOT NULL,
  `pwd` longtext CHARACTER SET latin1 NOT NULL,
  `ctime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `onethink_user`
--

INSERT INTO `onethink_user` (`id`, `name`, `pwd`, `ctime`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1380500857);

-- --------------------------------------------------------

--
-- 表的结构 `onethink_video`
--

CREATE TABLE `onethink_video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `desp` varchar(255) NOT NULL COMMENT '描述',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面地址',
  `video_id` int(10) unsigned DEFAULT '0' COMMENT 'file表id',
  `video_url` varchar(255) NOT NULL COMMENT '视频地址',
  `width` int(6) unsigned DEFAULT '640' COMMENT '视频宽度',
  `height` int(6) unsigned DEFAULT '480' COMMENT '视频高度',
  `auto` tinyint(1) unsigned DEFAULT '0' COMMENT '是否自动播放 1-是 0-不是',
  `preload` tinyint(1) unsigned DEFAULT '1' COMMENT '是否预加载 1-是 0-不是',
  `circle` tinyint(1) unsigned DEFAULT '0' COMMENT '循环播放 1-是 0-不是',
  `create_time` datetime DEFAULT NULL COMMENT '发布时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `onethink_video`
--

INSERT INTO `onethink_video` (`id`, `title`, `desp`, `cover`, `video_id`, `video_url`, `width`, `height`, `auto`, `preload`, `circle`, `create_time`, `update_time`) VALUES
(1, '[集锦]哥伦比亚2-1科特迪瓦 魔兽德罗巴替补难救主 140620', '123', 'http://g3.ykimg.com/1100641F4653A3302220BD1656D027787FAEA2-3B29-D2E2-1A8B-0A6C40C59DF7', 0, 'http://player.youku.com/player.php/sid/XNzI4OTA2ODM2/v.swf', 640, 480, 0, 1, 0, '2014-06-20 07:48:27', NULL);
