<?php

/**
 * 根据用户ID获取用户名
 * @param  integer $uid 用户ID
 * @return string       用户名
 */
function get_username($uid = 0){
    $name = M('User')->getFieldById($uid, 'name');
    return $name? $name : '无名';
}

function get_front_cache(){
	if(!S('front_cache')){
		$single_list = M('Article')->where('`type`=2')->field(true)->order('`create_time` DESC,`id` DESC')->select();
		$new_article = D('Article')->lists(NULL, '`create_time` DESC,`id` DESC', 1, true, 3);

		$category = M('cate')->where('status = 1')->order('sort asc')->select();
		$count = M('Article')->group('cate_id')->where('`type` = 1')->getField('cate_id, count(*) as num');
		foreach ($category as $key => $value) {
			$category[$key]['article_num'] = isset($count[$value['id']])? $count[$value['id']] : 0;
		}
	    $cate = D('Cate')->toFormatTree($category);

	    $list = M('Article')->where('`type` = 1')->order('`create_time` DESC,`id` DESC')->select();
		$date = $time = array();
		foreach ($list as $key => $value) {
			if($value['create_time'])
				$time[] = date('F Y', $value['create_time']);
		}
		$time = array_unique($time);
		foreach ($time as $key => $value) {
			$date[] = array(
				'text'=> $value,
				'link'=> date('Y/m', strtotime($value))
			);
		}

		$cache = array(
			'single_list' => $single_list,
			'new_article' => $new_article,
			'cate'	 	  => $cate,
			'date'		  => $date,
		);
		S('front_cache', $cache);
	}
	return S('front_cache');
}
