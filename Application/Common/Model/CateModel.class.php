<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Common\Model;
use Think\Model;

/**
 * 文档基础模型
 */
class CateModel extends Model{
	protected $_auto = array (
        array('ctime', 'time', self::MODEL_INSERT, 'function'),
        array('utime', 'time', self::MODEL_BOTH, 'function'),
	);

	protected function _after_select(&$result,$options){
		$count = M('Article')->group('cate_id')->getField('cate_id, count(*) as num');
        $cates = M('Cate')->getField('id,title');
		foreach ($result as $key => $value) {
			$result[$key]['article_num'] = isset($count[$value['id']])? $count[$value['id']] : 0 ;
            if($value['pid'] && isset($cates[$value['pid']])){
                $result[$key]['p_title'] =  $cates[$value['pid']];
            }

		}
	}

    protected function _after_update($data,$options) {
       S('front_cache', NULL);
    }

    protected function _after_insert($data,$options) {
       S('front_cache', NULL);
    }

	/**
     * 将格式数组转换为树
     *
     * @param array $list
     * @param integer $level 进行递归时传递用的参数
     */
    private $formatTree; //用于树型数组完成递归格式的全局变量
    private function _toFormatTree($list,$level=0,$title = 'title') {
        foreach($list as $key=>$val){
            $tmp_str=str_repeat("&nbsp;",$level*2);
            $tmp_str.="└";

            $val['level'] = $level;
            $val['title_show'] =$level==0?$val[$title]."&nbsp":$tmp_str.$val[$title]."&nbsp";
                // $val['title_show'] = $val['id'].'|'.$level.'级|'.$val['title_show'];
            if(!array_key_exists('_child',$val)){
                array_push($this->formatTree,$val);
            }else{
                $tmp_ary = $val['_child'];
                unset($val['_child']);
                array_push($this->formatTree,$val);
                   $this->_toFormatTree($tmp_ary,$level+1,$title); //进行下一层递归
            }
        }
        return;
    }

    public function toFormatTree($list,$title = 'title',$pk='id',$pid = 'pid',$root = 0){
        $list = list_to_tree($list,$pk,$pid,'_child',$root);
        $this->formatTree = array();
        $this->_toFormatTree($list,0,$title);
        return $this->formatTree;
    }
}
