包管理插件vundle 
================

安装
------------

`git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle`

然后修改配置文件

安装插件
-----------

进入vim以后,执行 `:BundleInstall`


使用说明
--------

`:h vundle`



terryma/vim-multiple-cursors
===========================================================================

> vim的多重光标选择插件,速度比较慢,有时会导致崩溃


#### 快捷键

* Ctrl + n ,选中光标处的单词(此时自动进入可视化模式),再次重复 Ctrl+n 则会继续选择与第一次选中的内容相同的文本,选择时不会考虑文本边界
* Ctrl + p ,撤销本次选择,并返回到上一次选择的位置
* Ctrl + x ,撤销本次选择,下一次选择从本次选择内容的后面开始选择

#### 内容操作

* 可以使用 `v`在一般模式和可视模式之间切换; 
* 多重选择完成后,可以使用 `hjklo$^` 改变选择范围 ;
* 在可视模式时,可以使用 `d,c,x,s` ;
* 在一般模式时,可以使用 `a,A,i,I` ;


#### 多行选择

当选取多行后, 按下 ctrl + n 则会在每行的最前面放置一个光标

Lokaltog/vim-easymotion
=============================================================================

> 一般模式下,光标快速定位插件,神器插件,emacs和IDEA有AceJump,
> 但是easymotion是motion操作,所以配合operator命令可以实现复制删除选择等操作

**<leader>**默认为`\`键,快捷键为两个`\`紧跟一个motion操作

|Mapping                     |Details                                       |
|----------------------------|----------------------------------------------|
|`<leader><leader>`f{char}   |Find {char} to the right.                     |
|`<leader><leader>`F{char}   |Find {char} to the left.                      |
|`<leader><leader>`t{char}   |Till before the {char} to the right.          |
|`<leader><leader>`T{char}   |Till after the {char} to the left.            |
|`<leader><leader>`w         |Beginning of word forward.                    |
|`<leader><leader>`W         |Beginning of WORD forward.                    |
|`<leader><leader>`b         |Beginning of word backward.                   |
|`<leader><leader>`B         |Beginning of WORD backward.                   |
|`<leader><leader>`e         |End of word forward.                          |
|`<leader><leader>`E         |End of WORD forward.                          |
|`<leader><leader>`ge        |End of word backward.                         |
|`<leader><leader>`gE        |End of WORD backward.                         |
|`<leader><leader>`j         |Line downward.                                |
|`<leader><leader>`k         |Line upward.                                  |
|`<leader><leader>`n         |Jump to latest "/" or "?" forward.            |
|`<leader><leader>`N         |Jump to latest "/" or "?" backward.           |


mattn/zencoding-vim
============================================================================

展开快捷键 `ctrl+y` 然后`,`号

跳转到下一个标签编辑位置: 输入`ctrl+y` 然后 `n`

跳转到上一个标签编辑位置: 输入`ctrl+y` 然后 `N`


surrond.vim
=============================================================================

> 超级方便的单词包围插件,不仅可以改变引号括号包围,还可以添加删除和改变xml标签包围

Normal mode
-----------

* ds  - 例如`ds'`表示把包围着的单引号移除
* cs  - 例如`cs'"`表示把包围着的单引号变成双引号,`cs'<div>`表示把包围着的单引号变成`<div></div>`包围
* ysi{textObj}  - 例如`ysiw"`表示用双引号包围
* ysa{textObj}  - 例如`ysaw"`表示用双引号包围
* ySi{textObj}  - add a surrounding and place the surrounded text on a new line + indent it
* ySa{textObj}  - add a surrounding and place the surrounded text on a new line + indent it
* yss - add a surrounding to the whole line
* ySs - add a surrounding to the whole line, place it on a new line + indent it
* ySS - same as ySs

### ys命令

> 光标定位在一个单词中间,`ysiw`后面根一个符号用于包围单词,跟`t<tag>`使用标签包围单词

> `yss`则包围整行内容,`ySs和ySS`包围整行内容,并将原内容换行并缩进

|Text         |Command        |New Text          |
|-------------|---------------|------------------|
|Hello world! |ysiw)          |Hello (world)!    |
|Hello world! |csw)           |Hello (world)!    |
|foo          |`ysiwt<html>`  |`<html>foo</html>`|
|foo quux baz |yss"           |"foo quux baz"    |

### cs命令

> cs后面根被替换掉的可以是一个符号`'"([{)]}`之一,如果是左括号,包围时内部会加一个空格,如果是右括号,包围时不加空格;
也可以是t表示一个xml标签,然后再根用于替换的符号或xml标签

|文本            |命令    |结果                 |
|----------------|--------|---------------------|
|`"Hello world!"`|`cs"'`  |`'Hello world!'`     |
|`"Hello world!"`|`cs"<q>`|`<q>Hello world!</q>`|
|`(123+456)/2`   |`cs)]`  |`[123+456]/2`        |
|`(123+456)/2`   |`cs)[`  |`[ 123+456 ]/2`      |
|`<div>foo</div>`|`cst<p>`|`<p>foo</p>`         |
|`foo!`          |`csw'`  |`'foo'!`             |
|`foo!`          |`csW'`  |`'foo!'`             |

可视化模式
-----------

* S   - in visual mode, add a surrounding


scrooloose/syntastic
===================

> 支持多种语言的语法检查插件,非实时检查,保存时检查

支持语言列表:

 Ada, AppleScript, Bourne shell, C, C++, C#, CoffeeScript, Coco, Coq, CSS, Cucumber, CUDA, 
 D, Dart, DocBook, Elixir, Erlang, eRuby, Fortran, Gentoo metadata, Go, Haml, Haskell, Haxe,
 HTML, Java, JavaScript, JSON, LESS, LISP, LLVM intermediate language, Lua, MATLAB, NASM, 
 Objective-C, Objective-C++, OCaml, Perl, Perl POD, PHP, Puppet, Python, reStructuredText, 
 Ruby, Rust, SASS/SCSS, Scala, Slim, Tcl, TeX, Twig, TypeScript, Vala, VHDL, xHtml, XML, 
 XSLT, YAML, z80, Zope page templates, zsh.

* 当保存后检测到错误,会在下方显示错误信息窗口
* 当光标移动到有错误的行,命令窗口会显示详细的错误信息
* 有错误的行都会在行号前面加上标记,错误和警告的颜色深浅不同
* 鼠标移动到有错误的行,会显示一个含有错误信息的balloon
* 错误会高亮显示



AndrewRadev/splitjoin.vim 
=======================

> 代码拆行合并工具， 支持的内容:

* **PHP arrays**
* Javascript object literals and functions
* Tags in HTML/XML
* CSS, SCSS, LESS style declarations.
* Various constructs in Coffeescript
* Various constructs in Ruby and Eruby
* Various constructs in Perl
* Various constructs in Python
* YAML arrays and maps
* Lua functions and tables
* Vimscript line continuations

#### 命令

拆行： `gS`

合并： `gJ`



vim-scripts/Align
========================================

> 无比强大的多行对齐插件,通常把自己常用的对齐模式设置成键位映射来使用

> h: Align

选项设置命令:
-------------

**AlignCtrl**命令用来为**Align**命令执行设置对齐选项

* AlignCtrl语法: `:AlignCtrl option [..list-of-separator-patterns..]`

    * options支持使用以下一个或多个字符`=Clrc-+:pPIWw`,各字符的作用如下:

         |@	 | @                                                                                   |
         |---|------------------------------------------------------------------------------------ |
         |=	 |all separator patterns are equivalent and active simultaneously. Patterns are regexp |
         |C	 |Cycle through separator patterns (ie. patterns are active sequentially)              |
         | < |左对齐分隔符                                                                         |
         | > |右对齐分隔符                                                                         |
         | \||居中对齐分隔符                                                                       |
         |l	 |左对齐分隔的文本                                                                     |
         |r	 |右对齐分隔的文本                                                                     |
         |c	 |居中对齐分隔的文本                                                                   |
         |p# |#是一个数字,在分隔符左边填充#个空格                                                  |
         |P# |#是一个数字,在分隔符右边填充#个空格                                                  |
         |I	 |保留第一行的前导空白字符,并将其用于对齐                                              |
         |W	 |保留每一行的前导空白字符                                                             |
         |w	 |不保留每行前导的空白字符hite space                                                   |
         |g	 | 声明第二个参数是一个选择pattern,只有 **符合**匹配的行才会被执行对齐                 |
         |v	 | 声明第二个参数是一个选择pattern,只有 **不符合**匹配的行才会被执行对齐               |

        * lrc支持使用下面的符号修饰

            * `-` 将这个位置的对齐符号视为field的一部分,即跳过这个对齐符号
            * `+` 重复最后一个对齐符号 (例如. lr+ 等价于 lrrrrrr... )
            * `:` 剩余的所有文本都作为一个field对待
            * `*` 用于提供一个函数来决定一个符合separator pattern的字符是否应该被跳过

        * 设置默认值: "Ilp1P1=" '='
        * 恢复默认设置: `:AlignCtrl default`

#### Align 命令:

|[range]Align  |   [..list-of-separators..]                                 |
|[range]Align! |   [AlignCtrl settings] [..list-of-separators..]            |


#### [..list-of-separator-patterns..]

分隔符支持使用正则表达式

示例:
-----

```
     +------------+---------------+--------------------+---------------+
     |  Original  |  AlignCtrl -l | AlignCtrl rl+      | AlignCtrl l:  |
     +------------+---------------+--------------------+---------------+
     | a=bb=ccc=1 |a=bb   = ccc=1 |  a = bb  = ccc = 1 |a   = bb=ccc=1 |
     | ccc=a=bb=2 |ccc=a  = bb=2  |ccc = a   = bb  = 2 |ccc = a=bb=2   |
     | dd=eee=f=3 |dd=eee = f=3   | dd = eee = f   = 3 |dd  = eee=f=3  |
     +------------+---------------+--------------------+---------------+
     | Alignment  |l        l     |  r   l     l     l |l     l        |
     +------------+---------------+--------------------+---------------+

     +-------------+------------------+---------------------+----------------------+
     |   Original  | AlignCtrl = = + -| AlignCtrl = =       | AlignCtrl C = + -    |
     +-------------+------------------+---------------------+----------------------+
     |a = b + c - d|a = b + c - d     |a = b + c - d        |a = b         + c - d |
     |x = y = z + 2|x = y = z + 2     |x = y         = z + 2|x = y = z     + 2     |
     |w = s - t = 0|w = s - t = 0     |w = s - t     = 0    |w = s - t = 0         |
     +-------------+------------------+---------------------+----------------------+


     +-----------------+--------------------+---------------------+---------------------+
     |    Original     |    AlignCtrl <     |     AlignCtrl >     |     AlignCtrl |     |
     +-----------------+--------------------+---------------------+---------------------+
     | a - bbb - c     |a   -   bbb -   c   | a     - bbb   - c   | a    -  bbb  -  c   |
     | aa -- bb -- ccc |aa  --  bb  --  ccc | aa   -- bb   -- ccc | aa  --  bb  --  ccc |
     | aaa --- b --- cc|aaa --- b   --- cc  | aaa --- b   --- cc  | aaa --- b   --- cc  |
     +-----------------+--------------------+---------------------+---------------------+
```

配置项
-----------

g:Align_xstrlen 用于设置插件计算字符串长度的方式,值可以是: 0,1,2,3(根据显示宽度计算)或一个字符串(自定义的计算字符串长度的函数)


键位映射
------------------


文本编辑常用的

* `\anum`:  对齐美国风格的数字(.号作为小数点)
* `\aenum`: 对齐欧洲风格的数字(,号作为小数点)
* `\aunum`: 对齐美国风格的数字(.号作为小数点)
* `\tsp` :  左对齐空格分隔的表格(对齐方法注释很有用)
* `\tsq` :  左对齐空格分隔的表格,引号包围的内容作为一个单元格(即忽略引号中的空格)
* `\Tsp` :  右对齐空格分隔的表格
* `\tml` :  对齐 multi-line continuation marks, shell脚本中很有用
* `\tab` :  align a table based on tabs (converts to spaces)


php开发常用的

* `\abox`:  围绕选择的文本绘制一个 C-style 注释块
* `\acom`:  对齐行尾注释
* `\adcom`: 对齐行尾注释,忽略没有内容的纯注释行
* `\a,`  :  split用`,`号分割的变量定义,支持c语言中的类型定义和js中的var定义(var a,b,c;)
* `\a?`  :  对齐三元表达式
* `\tx`  :  左对齐 "x" 代表以下符号之一: `,:<=@|#`
* `\Tx`  :  右对齐 "x" 代表以下符号之一: `,:<=@|#`
* `\ts,` :  like `\t,` but swaps whitespace on the right of the commas to their left
* `\ts:` :  like `\t:` but swaps whitespace on the right of the colons to their left
* `\ts<` :  like `\t<` but swaps whitespace on the right of the less-than signs to their left
* `\ts=` :  like `\t=` but swaps whitespace on the right of the equals signs to their left

C开发常用的

* `\a(`  :  aligns ( and , (useful for prototypes)        
* `\a<`  :  对齐 `<< and >>` for c++
* `\a=`  :  对齐 `:=` 
* `\adec`:  对齐类型声明(C语言中用到)
* `\adef`:  对齐常量声明(C语言中用到)
* `\afnc`:  对齐函数声明(C语言中用到)

junegunn/vim-easy-align
=======================

> 简单的对齐插件

用法:

1. 首先在可视化模式中选择文本
2. 然后进入命令模式 :`'<,'>EasyAlign<Enter>`
3. 此时默认为左对齐模式,按`<Enter>`可以在右对齐模式之间切换,按下之后
4. 输入一个数字,告诉插件以第几个对齐符进行对齐,默认不输入为1,*表示所有对齐符,负数表示从尾部倒数
5. 然后输入一个对齐符号,告诉插件以哪种类型字符对齐;插件支持空格和`=,.,|:}`几种对齐字符,其中=号表示单个或`=, ==, !=, +=, &&=`,



scrooloose/nerdtree
===============================================================================================

树形目录插件


#### 命令:

`:NERDTreeToggle`  显示/隐藏tree

`:NERDTreeFind`  把光标移动到当前文件在tree中的位置

`:NERDTreeCWD`   把tree的根目录切换到当前目录

`:NERDTreeFromBookmark <bookmark>` 以bookmark目录为tree的根目录打开tree

#### 键绑定


文件打开

* t 在新tab中打开光标位置的文件,如果光标位置是目录,则会在新tab窗口中以这个目录为根目录打开tree
* T 与t一样,只是光标停了在当前位置不变
* o 在上一此活动的窗口打开文件,光标移动到上一次活动的窗口
* i 在水平分割的新窗口中打开文件,光标移动到新窗口中
* s 在垂直分割的新窗口中打开文件,光标移动到新窗口中
* gi 与i一样,只是光标停了在当前位置不变
* go 打开光标位置的文件,但不切换窗口到文件中
* gs 垂直分割的新窗口中打开文件,光标位置保持不变

Tree管理

* o 展开/收起光标位置的目录
* O 递归展开光标位置的目录
* x 收起光标位置文件/目录所在的目录
* X 递归收起光标位置目录的所有子目录
* r 刷新选择的目录
* R 刷新整个tree
* A 最大化/最小化tree窗口
* m 打开文件/目录菜单,用esc退出
* q 关闭tree窗口

Tree显示过滤

* f 切换是否使用文件过滤
* F 显示/隐藏所有文件
* I 显示/隐藏隐藏文件
* B 显示/隐藏bookmark
* D 删除当前选择的书签

Tree根目录切换

* C  把tree的根目录设为光标所在目录
* CD 把tree的根目录设为vim的工作目录
* cd 把vim的工作目录设为光标所在位置的目录
* u 使tree的根目录上移一层
* U 使tree的根目录上移一层,并保存原来的目录展开情形和光标位置不变

Tree光标导航

* p 光标跳到父目录上
* P 光标跳到根目录上
* J 光标跳到同级的最后一个文件或目录上
* K 光标跳到同级的第一个文件或目录



常用设置:

`let NERDTreeIgnore=['\.vim$', '\~$']` 使用正则表达式数组设置忽略的文件,可以使用 `f` 切换这个设置是否生效

#### Q&A

Q. 如何在vi启动时自动打开tree?

A. .vimrc: `autocmd vimenter * NERDTree`

Q. 如果只在vi启动时(没有指定文件)打开tree?

A. .vimrc `autocmd vimenter * if !argc() | NERDTree | endif`

Q. How can I close vim if the only window left open is a NERDTree?

A. `autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif`


oinksoft/proj.vim
==================

**:ProjAdd**  将当前工作目录设为一个项目

**:Proj {project}**  打开一个项目



vim-scripts/bufexplorer.zip
================================================================================================

> 该插件默认键位绑定与brolink.vim冲突

* `<leader>be` 在当前窗口打开BufExplorer窗口
* `<leader>bv` 用垂直线分隔出BufExplorer窗口
* `<leader>bs` 用水平线分隔出BufExplorer窗口


buffer窗口支持的命令:

* enter        在当前窗口打开缓冲区文件
* o            在当前窗口打开缓冲区文件
* t            在另一个tab打开缓冲区文件 
* B            切换显示所有的buffer还是仅当前tab上的buffer
* d            从buffer列表删除,但在unlist列表中还能看到
* D            在哪里都看不到这个buffer了
* f            Toggles whether you are taken to the active window when selecting a buffer or not.
* p            切换显示完整路径还是目录与文件分开的格式
* q            关闭BufExplorer
* R            目录相对/绝对路径切换
* r            列表反序
* s            切换列表排序方式 
* S            切换列表排序方式(反序)
* T            Toggles to show only buffers for this tab or not.
* u            显示unlist的列表
* leftmouse    在当前窗口打开缓冲区文件
* shift-enter  在另一个tab打开缓冲区文件
 
 
状态共且以下几种:

```
- 不活动的缓冲
h 隐藏的缓冲
% 当前缓冲
# 其它缓冲
+ 文件被编辑而未保存
```
 
winresizer
======================================================================================================

改变编辑区大小

:WinResizerStartResize

然后使用 hjkl 调整大小


terryma/vim-expand-region
=============================================================================================================

> 在一般模式下,使用 + - 号 按定义的文本对象向上选择或取消上一次选择

可以改变插件默认的文本对象,插件默认使用的文本对象如下:


```
let g:expand_region_text_objects = {
      \ 'iw'  :0,
      \ 'iW'  :0,
      \ 'i"'  :0,
      \ 'i''' :0,
      \ 'i]'  :1, 
      \ 'ib'  :1,
      \ 'iB'  :1,
      \ 'ip'  :0,
      \ }
```


可以为插件自定义文本对象,方法示例如下:

ii,ai是插件提供的文本对象

```
call expand_region#custom_text_objects({
      \ "\/\\n\\n\<CR>": 1, 
      \ 'a]' :1, 
      \ 'ab' :1,
      \ 'aB' :1,
      \ 'ii' :0,
      \ 'ai' :0, 
      \ })
```


也可以针对不同类型的语言,使用不同的策略:

以ruby语言为例,以下设置仅对rubu生效

```
let g:expand_region_text_objects_ruby = {
      \ 'im' :0, " 'inner method'. Available through https://github.com/vim-ruby/vim-ruby
      \ 'am' :0, " 'around method'. Available through https://github.com/vim-ruby/vim-ruby
      \ }
```


tpope/vim-repeat
===========================================================================================

> 让vim的 `.`重复操作命令支持以下插件命令的重复操作,好像有时候会失效

* surround.vim
* speeddating.vim
* abolish.vim
* unimpaired.vim
* commentary.vim

rst_table
==========================================

> <http://www.vim.org/scripts/script.php?script_id=4327>

!!!!键映射  `\,c` 创建表格;`\,f`修整表格

> 无需选择文本,直接光标定位在表格的任意位置执行以上按键即可

> 基于字符宽度格式,对多字节文本支持良好

```
nombre    apellido    edad 
pepe        zarate  28 
toto        garcia      29 
```

变为

```
+--------+----------+-------+
| nombre | apellido | edad  |
+========+==========+=======+
| pepe   | zarate   | 28    |
+--------+----------+-------+
| toto   | garcia   | 29    |
+--------+----------+-------+
```


dhruvasagar/vim-table-mode
==========================================================================

> 强大的文本表格编写插件,但与emacs和sblimetext的表格插件比还有一定距离  

> 多字节文字支持也不太好

> h: table-mode.txt

命令
----------

:TableModeToggle 切换关闭和开启table模式

配置项
------

```
" 是否启用该插件
let g:loaded_table_mode = 1

" 是否使用border
let g:table_mode_border = 1

" 单元格角落符号
let g:table_mode_corner = '+'

" 列分隔符,该定义也是在插入模式下开始创建表格的其实符号
let g:table_mode_separator = '|'

" 行分隔符
let g:table_mode_fillchar = '-'

" 定义是否永久开启table mode
let g:table_mode_always_active = 0

" 定义键映射的命令前缀
let g:table_mode_map_prefix = '<Leader>t'

" Use this option to define the delimiter which used by table-mode-command-tableize
let g:table_mode_delimiter = ','

" Use this option to define the mapping to invoke |:Tableize| with input parameter.
" This option will ask for command-line input {pattern} that defines the delimiter. >
let g:table_mode_tableize_op_map = '<Leader>T'

" 定义cell text object
let g:table_mode_cell_text_object = 'tc'
```

后缀命令定义
--------------------------------------------------------

```
" 定义toggle table mode的命令键
let g:table_mode_toggle_map = 'm'

" Use this option to define the mapping to invoke |:Tableize| with default delimiter, i.e. |:Tableize| without input. >
let g:table_mode_tableize_map = 't'


" 重新对齐列的命令后缀
let g:table_mode_realign_map = 'r'

" 定义删除整个row的命令后缀
let g:table_mode_delete_column_map = 'dd'

" 定义删除整个col的命令后缀
let g:table_mode_delete_column_map = 'dc'

"  Use this option to define the mapping for invoking |TableAddFormula| command
let g:table_mode_add_formula_map = 'fa'

" Use this option to define the mapping for invoking |TableEvalFormulaLine| command. >
let g:table_mode_eval_expr_map = 'fe'
```



xolox/vim-session
===================

> 依赖插件 xolox/vim-misc

```
let g:session_autoload="yes"
let g:session_autosave="yes"
let g:session_autosave_periodic=1
```

