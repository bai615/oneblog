vim帮助文件
================

vim的帮助文件是一个普通的文本文件,只需要在文本文件的末尾声明类型为help类型,或者直接在命令模式设置

帮助文件可以设置 内容锚点,并在锚点和目标之间跳转, 使用竖线包围一个字符串可以定义一个锚点,使用*号包围一个相同的字符串可以定义一个目标

vim在渲染help类型时,会自动隐藏竖线和*号,把光标放在锚点上,按 `Ctrl + ]` 可以跳至目标,然后按下 `Ctrl + T` 或 `Ctrl + O` 可以跳回锚点

:help  可以打开vim帮助,并且会自动在其中列出所有安装的插件的帮助文件的锚点

进阶帮助文档

|文档         | 介绍                                               |
|-------------|----------------------------------------------------|
|cmdline.txt  | Command-line editing                               |
|options.txt  | description of all options                         |
|pattern.txt  | regexp patterns and search commands                |
|map.txt      | key mapping and abbreviations                      |
|tagsrch.txt  | tags and special searches                          |
|quickfix.txt | commands for a quick edit-compile-fix cycle        |
|windows.txt  | commands for using multiple windows and buffers    |
|tabpage.txt  | commands for using multiple tab pages              |
|syntax.txt   | syntax highlighting                                |
|spell.txt    | spell checking                                     |
|diff.txt     | working with two to four versions of the same file |
|autocmd.txt  | automatically executing commands on an event       |
|filetype.txt | settings done specifically for a type of file      |
|eval.txt     | expression evaluation, conditional commands        |
|fold.txt     | hide (fold) ranges of lines                        |



编辑环境设置
===============

支持的字符集
-------------

* cp936	    simplified Chinese (Windows only)
* euc-cn	simplified Chinese (Unix only)
* cp950	    traditional Chinese (on Unix alias for big5)
* big5	    traditional Chinese (on Windows alias for cp950)
* euc-tw	traditional Chinese (Unix only)
* utf-8	    32 bit UTF-8 encoded Unicode (ISO/IEC 10646-1)

设置方式
----------

> `set option`  对于没有值的option,都可以使用no前缀来关闭该option

> `set option = value` ,value字符串中如果有空格 | \ ,需要转义这些字符

> `set option += value` ,如果value为数字,则表示在原值上增加,如果value为字符串,则表示在原字符串append

> `set option ^= value` ,如果value为数字,则表示在原值上相乘,如果value为字符串,则表示在原字符串prepend

> `set option -= value` ,如果value为数字,则表示在原值上增减,如果value为字符串,则表示在原字符串中删除找到的字符串

> `set`可以简写为`se` , 可以用空格分隔一次设置多个option

> 可以使用 `t_`作为前缀设置terminal选项

> 一些选项是对window或buffer生效的,也就是不同的window和buffer有自己的生效的选项值,不会影响其他window和buffer

| option       | 简写 | value              | 说明                                                                                  |
|--------------|------|--------------------|---------------------------------------------------------------------------------------|
| autochdir    | acd  | -                  | 自动切换工作目录到当前打开的文件所在目录                                              |
| autoindent   | -    | -                  | 自动以上一行的缩进值缩进新行                                                          |
| autoread     | ar   | -                  | 当文件被外部程序改变,自动重新载入                                                     |
| background   | bg   | 值为 dark 或 light |                                                                                       |
| buftype      | bt   | char               | nofile/nowrite/help                                                                   |
| cmdheight    | ch   | int                | 命令窗口高度,默认为1                                                                  |
| encodeing    | enc  | char               | vim内部字符集,不会影响文件本身的字符集,建议设置为utf-8                                |
| fileencoding | fenc | char               | 设置当前buffer的文件字符集                                                            |
| fileencodings| fencs| char               | 设置文件字符集列表,建议设置为"ucs-bom,utf-8,euc-cn,cp936,cp950,big5,default,latin1"   |
| filetype     | ft   | char               | 设置文件的类型                                                                        |
| gdefault     | gd   | -                  | 设置文:substitute命令的g修饰符表示非多次匹配,不使用g为多次匹配                        |
| guifont      | gfn  | char               | 设置gui环境的字体和字号                                                               |
| guifontset   | gfs  | char               | 设置gui环境的字体列表,第一个字体用于显示英文,第二个字体用于显示其他语言               |
| hlsearch     | -    | -                  | 高亮搜索结果                                                                          |
| ignorecase   | ic   | -                  | 搜索的pattern表达式忽略大小写                                                         |
| incsearch    | -    | -                  | 搜素时实时高亮搜索结果,并将buffer跳转到结果                                           |
| linespace    | lsp  | int                | 行距                                                                                  |
| magic        | -    | -                  | 与搜索时的正则表达式特殊符号有关                                                      |
| matchpairs   | mps  | char               | 设置%号匹配的符号, 使用+=进行设置,默认值:"(:),{:},[:]"                                |
| modifiable   | ma   | -                  | 如果设置了,缓冲区不允许编辑                                                           |
| number       | nu   | -                  | 设置是否显示行号                                                                      |
| scroll       | scr  | int                | 设置c-u,c-d每次滚动的行数,默认值窗口的一半                                            |
| scrollbind   | scb  | -                  | 如果两个窗口都设置了该项,那么它们将同步滚动                                           |
| scrolloff    | so   | int                | 在光标上方或下方保留的最小行数,默认为0                                                |
| scrollopt    | sbo  | char               | 设置scrollbind的行为,值有:ver,hor,jump三种,默认值为:"ver,jump"                        |
| shiftwidth   | st   | int                | 使用>> <<命令时的缩进量默认为8,一般需要设置为4                                        |
| smartcase    | scs  | -                  | -                                                                                     |
| smartindent  | -    | -                  | 智能缩进                                                                              |
| textauto     | -    | -                  | -                                                                                     |
| textwidth    | -    | int                | 默认值为0,不自动断行                                                                  |
| showcmd      | -    | -                  | 在右下角显示输入的键                                                                  |
| cursorline   | cul  | int                | 高亮光标所在行                                                                        |
| wrapmargin   | wm   | int                | 当一行的内容距离视窗右边界达到该值后,自动换行,必须将textwidth设为0                    |
| wrap         | -    | -                  | 当设置时,文本到视窗边界后会软断行                                                     |


modeline
--------------

> 模式行,文件的最后一行可以用来为这个文件设置专属的option

在文件末尾加一行设置文本即可,设置项用冒号分隔:` vim:tw=78:sts=8:ts=4:sw=8:noet:ft=help:`

模式行的两端可以有任意字符串,例如可以放在注释中以不影响程序脚本: `/* vim: set ai tw=75: */`

!!!! 注意:模式行的 `vim` 前至少要有一个空格,如果发现一个错误,则之后的选项不再解析,只支持set命令


窗口管理
===================

window管理
-------------

* ctrl-w s  水平分隔窗口打开一份文件副本

* ctrl-w v  垂直分隔窗口打开一份文件副本

* ctrl-w n 水平分隔窗口打开一个空buffer

* ctrl-w o 在屏幕中只保留当前窗口,所有的其他窗口都关闭

* :new [file] 水平分隔窗口打开一个空buffer

* :vnew [file] 垂直分隔窗口打开一个空buffer

* ctrl-w q  关闭窗口


tabpage 
-----------

#### 新建和关闭


* :tabnew   新建一个空tabpage
* :tabc[!]  关闭当前tabpage
* :tabc[!] {count} 关闭某个tabpage
* :tabo[!] 关闭其他所有tabpage


#### 下一个tabpage

* :tabn[ext]
* `<C-PageDown>`
* gt

#### 到第count个tab

* :tabn[ext] {count}
* `{count}<C-PageDown>`
* {count}gt


#### 上一个tabpage

* :tabp[revious]
* `<C-PageUp>`
* gT

#### 上count个tabpage

* :tabp[revious] {count}
* `{count}<C-PageUp>`
* {count}gT

#### 第一个和最后一个tabpage

* :tabr[ewind]
* :tabfir[st]
* :tabl[ast]	Go to the last tab page.

vim中执行shell
=================

命令的执行方式
-------------

1. 使用linux的`ctrl+z`可以将vi放至后台(Gvim则最小化)，使用`fg`可以将后台程序放回前台

2. `:!{shell command}`, 不包括大括号: 直接执行shell命令,结果显示在vim命令窗口输出区域：  

3. `:r !{shell command}` , 执行命令，并将命令输出粘贴在光标下面

4. `[range]!{shell command}` 将范围内的内容作为shell command的输入,并用命令输出替换掉range内容

5. `[range] w !{shell command}` 将范围内的内容作为shell command的输入,把输入显示在命令窗口,而不会替换range内容

{r: ! 号两边的空格都可以省略, w 左边的空格不能省略}

{r:只能使用真实命令,不能使用别名命令}

特殊参数
-----------

1. `%` 在命令参数中,可以使用 % 号传入当前文件相对于当前working directory的相对文件路径,例如:`:r !echo %`
2. `<cword>` 光标所在处的word
3. `<cWORD>` 光标所在处的WORD

#### 文件名修饰符

特殊参数`%`后面可以使用修饰符:
	
* :p	必须是第一个修饰符,转为绝对路径.  例如:`:r !echo %:p`	
* :~	如果是home目录下的文件,转为 `~/`格式 的路径 
* :.	相对于当前工作目录的相对路径.
* :h	去掉文件名(最后一个/及后面的字符),只保留路径,如果文件末尾是`/`,则移除的是`/`
* :t	去除路径,只保留文件名  Must precede any :r or :e.
* :r	去除最后一个后缀 (the last extension removed).	如果文件是.开头没有其他后缀,不会移除
* :e	保留文件的后缀(不含.号).如果文件没有后缀,或者只有一个.开头,结果是空.
* :s?pat?sub?  使用正则替换修改文件路径,只替换第一次的匹配
* :gs?pat?sub? 使用正则替换修改文件路径,替换每一次的匹配


Examples, when the file name is "src/version.c.gz": 

```
  :p			/home/mool/vim/src/version.c.gz
  :e						     gz
  :e:e						   c.gz
  :e:e:e					   c.gz
  :e:e:r					   c
  :r				       src/version.c
  :r:e						   c
  :r:r				       src/version
  :r:r:r			       src/version
```

多命令执行
===========

管道重定向等

输出到新窗口或tabpage中:`:tabe +r\ !php\ -m<cr>`



命令模式输入技巧
================

#### 命令修改

在命令区移动光标：方向键，`Cttl+left/right`,`ctrl+b,ctrl+e`

删除一个单词：`ctrl+w`

删除输入的所有命令字符：`ctrl+u`

在插入和覆盖模式切换：`<insert>`

退出命令输入界面：`ctrl+c`

#### 自动完成

对于文件和目录以及命令的输入支持自动完成：`tab`(循环显示候选项)，`ctrl+d`（显示可补全的候选项列表）

补全到共同字符：例如index.php,index.html，:set i 按下ctrl+l，自动补全为：set index.

#### 命令历史

直接使用上下键调用命令历史

输入命令的几个字符，然后使用上下键，调用匹配的命令历史

:history 列出命令历史记录

#### 命令窗口

命令模式只能输入一行命令，命令窗口可使用标准的编辑窗口编译过去输入过的命令，编辑完成后回车即可执行该行命令

#### 文件浏览器

`:pwd` 显示vi目前的工作目录

`:cd {path}`切换vi的工作目录

`:ls` 显示缓冲区的文件列表，每个文件前面有一个数字标识

`:buffer {n}` 编辑相应的标识文件

`:edit {path}`，如果path是目录，则会打开文件浏览器，移动光标到需要编辑的文件，然后按下

`<enter>` 在当前窗口编辑该文件

`o`  打开一个水平分割窗口显示该文件

`v`  打开一个垂直分割窗口显示该文件

`t` 在新tab中显示该文件

#### 前往目标

`gf`，当光标停留位置的内容是一个文件或网址上面，gf则会前往这个目标


格式化
===========

按textwidth格式化
---------------------

`:set textwidth=30`,设定编辑区域宽度

`gq`,重新格式化选择的内容(visual模式选择)(gq是一个opreator命令)

`gqap`,重新格式化一段(ap是段落的text object)

对齐方式格式化
--------------

`:{range}center [width]`,居中对齐，例如 :1,3center 20 表示第1～3行在20个字符宽度居中对其

`:{range}right [width]`,右对其

`:{range}left [margin]`,左对齐，左对齐无需宽度指定，可以指定magin


 

### vi中的正则

`:set ignorecase`,全局不区分大小写

`:set noignorecase`,全局区分大小写

`\Cpattern` ,表达式区分大小写

`\cpattern`,表达式不区分大小写

vi将 ` . * ^ $  [ ] ` 作为正则符号解释,而`( | ) + ?` 则作为普通字符解释，以方便字节搜索，所以在正则中要这几个符号，需转义`\(,\)，\|，\+，\?`
量词限制只需转义正大括号\{m,n}

|量词     | 匹配次数               |
|-------- |----------------------- |
|\\{,4}   | 0, 1, 2, 3 or 4        |
|\\{3,}   | 3, 4, 5, etc.          |
|\\{0,1}  | 0 or 1, same as \=     |
|\\{0,}   | 0 or more, same as *   |
|\\{1,}   | 1 or more, same as \\+ |
|\\{3}    | 3                      |

|item     | equivalent         |
|-------- | ------------------ |
|\s       |                    |
|\S       |                    |
|\d       | [0-9]              |
|\D       | [^0-9]             |
|\l       | [a-z]              |
|\L       | [^a-z]             |
|\u       | [A-Z]              |
|\U       | [^A-Z]             |
|\x       | [0-9a-fA-F]十六进制字符|
|\X       | [^0-9a-fA-F]非十六进制字符|
|\w       | [a-zA-Z_]           |
|\W       | [^a-zA-Z_]          |

单词分界`\<  \>`

### 常用正则表达式：

`/^$/`:匹配空行

`/^/`匹配任意行

### 宏

`q{register} ` 录制宏

`@q{register}`  播放宏

`@@ ` 播放上一次播放的宏

### 替换

`:[range]substitute/from/to/[flags]`

`:[range]s/from/to/[flags]`

如果没有设置flags，默认每行只替换第一个搜到的结果，flags可使用 `g` 替换所有的结果，使用`c` 替换前询问 (y,n,a,q,l)

from部分可以是一个正则表达式，from和to里面如果有`/`，应转义或者使用其他符号代替`/`作为分隔符

+ range 用法：

	* 行号	an absolute line number
	* .		当前行
	* $		最后一行
	* %		整个文件
	* 't		t标记位置处
	* /{pattern}[/]	光标往下匹配pattern的行
	* ?{pattern}[?]	光标往上匹配pattern的行

    * 可以使用+和-号对位置进行偏移设置

+ 例子:

    - 如果不指定，表示在当前行执行替换。
    - 第1～5行 :1,5s/this/that/g    ；
    - 第54行 :54s/President/Fool/  ；
    - 当前行到最后一行：:.,$s/yes/no/ 或:%s/yes/no/
    - 当前行加3到最后一行减5  :.+3,$-5；两个标记行之间 :'t,'b
    - 如果是visual模式选择了内容，进入命令模式后执行的操作就会只对选择经过的行有效，range自动设为了:`'<,'>`
	

### global command

`:[range]g/{pattern}/{command}`

这条命令可以让你使用正则寻找匹配，然后对匹配目标所在行执行command。range用法和替换命令相同，如果不指定表示在全文应用正则。


#设置归纳


:set maplocalleader = ","    "  设置`<leader>`键

:set ft = pandoc              " 设置当前文档类型,支持的类型:text,help,markdown....

一个设置的值如果含有空格,则空格需要使用反斜线转义,例如: `setlocal equalprg=pandoc\ -t\ markdown\ --reference-links`

快捷键映射
------------

> h: user_40.txt

* :map		Normal, Visual and Operator-pending
* :vmap		Visual
* :nmap		Normal
* :omap		Operator-pending
* :map!		Insert and Command-line
* :imap		Insert
* :cmap		Command-line


例如:

" Ctrl + L 插入模式下光标向右移动

imap <c-l> <Right>


映射命令可以是该模式下所有有效的按键或vim命令

<Bar> 表示竖线|,在映射中用来表示管道
<Space> 表示空格
<CR>表示回车键 <BS>表示退格键

<Up><Down><Left><Right> 上下左右

<c-a> 组合键ctrl+a(不区分大小写)

<a-a> 组合键alt+a(区分大小写)

根据文件类型设置键位映射
------------------------

```
au BufNewFile,BufRead,BufEnter *.md setf markdown
autocmd FileType markdown  nmap <buffer> <c-a-r> <ESC>:!~/bin/markdown.php -v -f %:p<cr>
autocmd FileType markdown  imap <buffer> <c-a-r> <ESC>:!~/bin/markdown.php -v -f %:p<cr>
autocmd FileType php nmap <buffer> <c-a-r> <ESC>:tabe +r\ !php\ -m<cr>
autocmd FileType php imap <buffer> <c-a-r> <ESC>:tabe +r\ !php\ -m<cr>
```
