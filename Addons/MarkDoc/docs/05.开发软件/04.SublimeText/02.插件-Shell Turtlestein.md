Shell Turtlestein
=================


![input a shell command](https://img.skitch.com/20120207-m5g6dkrbhj2ed9pt9wn42rg7yc.jpg)


![command output](https://img.skitch.com/20120207-e4wwrfr7s8tyds7rptfesxi65p.jpg)

输入 输出 处理
------------------------

> 允许使用unix中的 管道和重定向操作符  `|` `>` 来处理输入输出

  * 把当前视图中选择的文本通过管道传递给另一个命令，只需使用 `|`作为命令的开头，如果没有选择文本，则整个文件内容传递给管道命令
    例如： `| sort`
  * 把一个管道命令的输出传回视图，则只需要在命令尾部使用管道符，例如：
    `| sort |`.
  * 把一个命令的输出重定向到一个文件，则只需要在尾部使用输出重定向符号 `>` 
    例如：  `ls >` 或 `| sort >`


使用 snippets
--------------

> 你可以把常用的命令定义成snippets，所有的命名范围是`source.shell` 的snippets都可以在命令输入窗口中直接使用

> shell snippets保存在 `Sublime-Packages/User/Snippets/Shell/`下面

内容如下：

	<snippet>
		<content><![CDATA[bundle exec ]]></content>
		<tabTrigger>be</tabTrigger>
		<scope>source.shell,source.dosbatch</scope>
	</snippet>


[some examples](https://github.com/misfo/Sublime-Packages/tree/master/User/Snippets/Shell)


默认快捷键
-------------------

* {g:Ctrl + Shift + C (Cmd + Shift + C)}: 命令输入窗口 
* {g:Ctrl + Alt + Shift + C (Cmd + Alt + Shift + C)}: launch a terminal in the
  window's directory


可选配置项
----------------------

> 在插件的用户配置文件中，你可以覆盖以下配置项

* `surround_cmd`: 这是一个两个元素的数组，分别定义类附加在命令前和命令后的内容，例如： `["source ~/.profile && ", ""]`

* `exec_args`: 附加在命令后的参数，选项与 [build system](http://sublimetext.info/docs/en/reference/build_systems.html)相同，
	但是 `file_regex`, `line_regex`, `encoding`, `env`, `path` are the only options that make sense to use with this plugin.
	Arguments specified in the `cmd_settings` (see below) will override these
	defaults.
	
* `cmd_settings`: An array of configurations to use for commands that are
	executed.  The first configuration to match the command being run will be
	used.  The keys that each configuration should have are:
	
	* `cmd_regex`: A regex that must match the command for this configuration
		for this configuration to be used.
		
	* `exec_args` and `surround_cmd` override the settings described above for
		any matching command.

