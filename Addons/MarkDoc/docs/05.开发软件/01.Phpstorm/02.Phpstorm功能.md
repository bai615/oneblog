Project Settings
=====================

Schemas and DTDs
----------------

该功能可以自定义html文件的验证文件，已改变默认的html智能提示和自动完成功能。

### External Schemas and DTDs

该项用于将xml/xhtml文档的Schema命名空间或html文档的DTD映射到一个本地的schema文件上，
这样IDE在进行错误提示智能完成时，就将使用本地的schema文件

点击 “+” 号，添加一个新映射，在窗口上方的 URI 文本域输入欲进行映射的schema或DTD的网址，然后在下面
的 Schemas 选择一个PhpStorm内置的本地验证文件，或从 Explore中选择一个本地磁盘上的自定义schema文件，
支持rng,dtd,xsd文件，最好使用xsd文件。

#### 下面是几种常见文档的原始schema的URI

	html4 Transitional     http://www.w3.org/TR/html4/loose.dtd
	html4 Strict           http://www.w3.org/TR/html4/strict.dtd
	html4 Frameset         http://www.w3.org/TR/html4/frameset.dtd
	XHTML 1.0 Strict       http://www.w3.org/2002/08/xhtml/xhtml1-strict.dtd 
	XHTML 1.0 Transitional http://www.w3.org/2002/08/xhtml/xhtml1-transitional.dtd 
	XHTML 1.0 Frameset     http://www.w3.org/2002/08/xhtml/xhtml1-frameset.dtd 
	XHTML 1.0 Strict       http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd 
	XHTML 1.0 Transitional http://www.w3.org/2002/08/xhtml/xhtml1-transitional.xsd 
	XHTML 1.0 Frameset     http://www.w3.org/2002/08/xhtml/xhtml1-frameset.xsd 
	XHTML 1.1              http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd
	html命名空间			http://www.w3.org/1999/html
	xhtml命名空间			http://www.w3.org/1999/xhtml

### Default html language level

默认的html版本等级，即html文件没有指定schema时，所应用的schema，
你可以在 **Other doctype** 中指定预定义(URI)或自定义的schema(本地路径)
