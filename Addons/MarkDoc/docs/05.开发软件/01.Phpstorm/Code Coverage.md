什么是代码覆盖
====================

> 简单地说,Code Coverage表述这样一个问题:当程序执行时,是否这行代码被执行过? 通常,我们希望每一行代码都有用处,都会在某些时候被执行.如果一行代码永远不可能被执行到,就没有存在的必要.如果程序中存在大量这样的无效代码,那么这个程序显然不是优秀的. 通常我们不会在程序中书写Code coverage生成的语句,而是由单元测试脚本在执行测试时生成.


PHPStorm的Code Coverage支持
------------

### 配置

Xdebug是必须的,在 `Setting->Project settings->Code Coverage` 可以设置 代码覆盖生成时Phpstorm的行为,默认是弹出窗口由用户选择行为

配置Code Coverage的颜色,在Editor->color and fonts->general中设置  **Full line coverage** 和 **Uncovered line** 的颜色

### Run with Code Coverage

在测试脚本上右击选择 Run xxx with coverage ,运行结束后,phpstorm 可以自动捕捉到测试脚本生成的Code coverage,并打开Code Coverage Toobar.