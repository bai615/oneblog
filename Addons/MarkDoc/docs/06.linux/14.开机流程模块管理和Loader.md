centos启动流程
=====================

runlevel (启动运行等级) 
------------------------

0. - halt (系统直接关机)
1. - single user mode (单人维护模式，用在系统出问题时的维护，不需要root密码) 
2. - Multi-user, without NFS (类似底下的 runlevel 3，但无 NFS 服务) 
3. - Full multi-user mode (完整含有网络功能的纯文字模式) 
4. - unused (系统保留功能)
5. - X11 (与 runlevel 3 类似，但加载使用 X Window) 
6. - reboot (重新启动)


/etc/inittab配置
----------------

> {r:/etc/inittab是redhat衍生版的启动配置文件}

> 格式： `[配置项目]:[run level]:[init 的动作行为]:[命令项目]`

> 1. 配置项目：最多四个字节，代表 init 的主要工作项目，只是一个简单的代表说明。
> 2. run level：该项目在哪些 run level 底下进行的意思。如果是 35 则代表 runlevel 3 与 5 都会运行。
> 3. init 的动作项目：主要可以进行的动作项目意义有：
>     * initdefault ---代表默认的 run level 配置值
>     * sysinit     ---代表系统初始化的动作项目
>     * ctrlaltdel  ---代表 [ctrl]+[alt]+[del] 三个按键是否可以重新启动的配置
>     * wait        ---代表后面栏位配置的命令项目必须要运行完毕才能继续底下其他的动作
>     * respawn     ---代表后面栏位的命令可以无限制的再生 (重新启动)。举例来说，tty1 的 mingetty 产生的可登陆画面， 在你注销而结束后，系统会再开一个新的可登陆画面等待下一个登陆。
>     * 更多的配置项目请参考 man inittab 的说明。 
> 4. 命令项目：亦即应该可以进行的命令，通常是一些脚本文件


init 的处理流程 
----------------

* 先取得 runlevel 亦即默认运行等级(测试机为例，为 5 号)；
* 使用 /etc/rc.d/rc.sysinit 进行系统初始化
* 由於 runlevel 是 5 ，因此只进行『l5:5:wait:/etc/rc.d/rc 5』，其他行则略过
* 配置 [ctrl]+[alt]+[del] 这组的组合键功能
* 配置UPS不断电系统的 pf, pr 两种机制；
* 启动 mingetty 的六个终端机 (tty1 ~ tty6)
* 最终以 /etc/X11/perfdm -nodaemon 启动图形介面啦

启动系统服务与相关启动配置档
-----------------------------

>  /etc/rc.d/rc N & /etc/sysconfig

### 方式1

> 各运行级别启动的服务脚本存放位置 

例如：/etc/rc.d/rc3.d/ 里面的runlevel3运行级别的服务脚本，只有大写开头的才会启动，如果不希望某个服务开机启动，可以把开头的字母改成小写，以后需要执行时，再改成大写。
如果希望后安装的程序中某个运行级别自动启动，可以把这个程序做一个软链接（命名要以大写字母开头跟一个数字ID)到相应级别的目录中 （例如：/etc/rc.d/rc3.d/ ）

### 方式2：编辑 /etc/rc.d/rc.local 把启动命令写入该文件，这个脚本在启动的最后阶段执行。

服务启动过程日志文件 /var/log/messages

runlevel切换
------------

* 查看当前的runlevel `# runlevel`
* 临时切换runlevel `# init 3`

启动服务
---------

例如: `/etc/rc.d/init.d/sshd`

chkconfig/ntsysv
----------------

* 查看系统所有服务的运行级别配置情况 `# chkconfig --list` 
* 指定某个服务在某几个运行级别的开启状况 `# chkconfig --level 35 sshd on` 
* 方便的图形化运行级别服务设置程序 `ntsysv --level 35`
* 添加和删除服务 `# chkconfig [--add|--del] [服务名称]`,服务脚本文件必须存在于`/etc/init.d/`


启动故障解决
------------

### inittab配置错误

> 在grub菜单进入编辑，在kernel项最后加一项 init=/bin/bash，进行启动 启动后由于没有执行init所以只有根目录只读挂载，而其他分区都没有挂载，所以下一步就是 
 
* 读写模式重新挂载跟分区 # mount -o remount,rw /  
* 挂载其他分区# mount -a
* 修复/etc/inittab文件
* 重启

### 文件系统错误

* 取得bash
* 重新挂载根分区 # mount -o remount,rw /
* 如果是/etc/fstab问题，进行修复
* 如果是磁盘文件系统错误，执行fsck

### 严重故障修复

> 如果故障严重到怎么样都无法启动，只能使用安装光盘提供的F5"修复模式"了

* 进入修复模式后，硬盘上的系统被自动挂载到了/mnt/sysimage
* 然后切换进入故障系统后进行检查和修复# chroot  /mnt/sysimage

> 如果硬盘上已经安装有两个Linux系统，则只需要进入没有问题的系统，然后按以下步骤操作

* 按照故障系统的目录结构创建一个对应的用于挂载的目录结构
* 按照对应关系把故障系统挂载起来
* chroot切换系统到故障系统的挂载点
* 执行修复

ubuntu启动流程
=============

/etc/init/目录
--------------

> ubuntu 10.10以后系统启动配置文件放在 `/etc/init/`目录下,每一个文件配置了一个程序的启动脚本文件

> 参考文章:<http://www.cnblogs.com/cassvin/archive/2011/12/25/ubuntu_init_analysis.html>

Grub配置
===========

centos
--------

直接编辑: `/boot/grub/grub.conf`

```
[root@www ~]# vim /boot/grub/menu.lst
default=0 <==默认启动选项，使用第 1 个启动菜单 (title)
timeout=5 <==若 5 秒内未动键盘，使用默认菜单启动
splashimage=(hd0,0)/grub/splash.xpm.gz <==背景图示所在的文件
hiddenmenu <==读秒期间是否显示出完整的菜单画面(默认隐藏)
title CentOS (2.6.18-92.el5) <==第一个菜单的内容
    root  (hd0,0) <==核心文件所在的硬盘分区，无需关心目录，分区指定正确即可。
    kernel     /vmlinuz-2.6.18-92.el5     ro     root=LABEL=/1     rhgb quiet LANG=zh_CN.UTF-8   vga=333 
    initrd /initrd-2.6.18-92.el5.img
title Windows partition
    rootnoverify (hd0,0) <==不检验此分割槽
    chainloader +1
    makeactive <==配置此分割槽为启动碟(active)
```

kernel和initrd配置里面的文件是相对于/boot目录的。也就是说  /vmlinuz-2.6.18-92.el5 的绝对位置是/boot/vmlinuz-2.6.18-92.el5，
所以安装有多个linux的话，配置其他linux启动就需要到这个linux的boot目录下找到对应的文件，配置到grub中

* root=LABEL=/1  用来设置『Linux 的根目录所在分区的卷标 』, 也可以用设备名来设置，例如：root=/dev/sda1
* rhgb表示彩色，quiet表示安静模式
* LANG=zh_CN.UTF-8  语言配置为简体中文字符集为UTF-8
* vga=333 配置屏幕分辨率为1024*768  16bit

{r:如果配置有误，启动时候可以在grub菜单下按e键，进行修改}

### 从GRUB命令行完成启动

如果grub配置出现错误，导致无法进入linux引导流程，可以在grub菜单按c进入命令行手动引导。

```
grub>root (hd0,0)
grub>kernel /vm*  ro root=LABEL=/ rhgb quiet 3
grub>initrd /init*
```
上面kernel和initrd可以使用tab命令补全

ubuntu
---------

1. 编辑 `/etc/default/grub`
2. 执行 `update-grub`
3. 如果有其他需要,可直接编辑:`/boot/grub/grub.cfg`

grub相关命令:`grub-`

