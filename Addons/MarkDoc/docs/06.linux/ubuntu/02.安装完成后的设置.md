修改软件源
===========

网易源
--------

> `sudo  vi /etc/apt/sources.list`

!!!!!适用于 ubuntu 12.10/linux mint 14

	deb http://packages.linuxmint.com/ nadia main upstream import backport romeo
	deb-src http://packages.linuxmint.com/ nadia main upstream import backport romeo #Added by software-properties
	# deb http://archive.ubuntu.com/ubuntu/ quantal universe
	# deb http://archive.ubuntu.com/ubuntu/ quantal-updates main restricted universe multiverse
	# deb http://security.ubuntu.com/ubuntu/ quantal-security main restricted universe multiverse

	# deb http://archive.getdeb.net/ubuntu quantal-getdeb apps
	# deb http://archive.getdeb.net/ubuntu quantal-getdeb games
	# See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
	# newer versions of the distribution.
	deb http://mirrors.163.com/ubuntu/ quantal main restricted
	deb-src http://mirrors.163.com/ubuntu/ quantal main restricted

	## Major bug fix updates produced after the final release of the
	## distribution.
	deb http://mirrors.163.com/ubuntu/ quantal-updates main restricted
	deb-src http://mirrors.163.com/ubuntu/ quantal-updates main restricted

	## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
	## team. Also, please note that software in universe WILL NOT receive any
	## review or updates from the Ubuntu security team.
	deb http://mirrors.163.com/ubuntu/ quantal universe
	deb-src http://mirrors.163.com/ubuntu/ quantal universe
	deb http://mirrors.163.com/ubuntu/ quantal-updates universe
	deb-src http://mirrors.163.com/ubuntu/ quantal-updates universe

	## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu 
	## team, and may not be under a free licence. Please satisfy yourself as to 
	## your rights to use the software. Also, please note that software in 
	## multiverse WILL NOT receive any review or updates from the Ubuntu
	## security team.
	deb http://mirrors.163.com/ubuntu/ quantal multiverse
	deb-src http://mirrors.163.com/ubuntu/ quantal multiverse
	deb http://mirrors.163.com/ubuntu/ quantal-updates multiverse
	deb-src http://mirrors.163.com/ubuntu/ quantal-updates multiverse

	## N.B. software from this repository may not have been tested as
	## extensively as that contained in the main release, although it includes
	## newer versions of some applications which may provide useful features.
	## Also, please note that software in backports WILL NOT receive any review
	## or updates from the Ubuntu security team.
	deb http://mirrors.163.com/ubuntu/ quantal-backports main universe multiverse restricted
	deb-src http://mirrors.163.com/ubuntu/ quantal-backports main universe multiverse restricted

	deb http://mirrors.163.com/ubuntu/ quantal-security main restricted
	deb-src http://mirrors.163.com/ubuntu/ quantal-security main restricted
	deb http://mirrors.163.com/ubuntu/ quantal-security universe
	deb-src http://mirrors.163.com/ubuntu/ quantal-security universe
	deb http://mirrors.163.com/ubuntu/ quantal-security multiverse
	deb-src http://mirrors.163.com/ubuntu/ quantal-security multiverse

	## Uncomment the following two lines to add software from Canonical's
	## 'partner' repository.
	## This software is not part of Ubuntu, but is offered by Canonical and the
	## respective vendors as a service to Ubuntu users.
	deb http://archive.canonical.com/ubuntu quantal partner
	deb-src http://archive.canonical.com/ubuntu quantal partner

	## This software is not part of Ubuntu, but is offered by third-party
	## developers who want to ship their latest software.
	deb http://extras.ubuntu.com/ubuntu quantal main
	deb-src http://extras.ubuntu.com/ubuntu quantal main


增加ppa源
--------

> 搜狗输入法和oracle-java需要从ppa源安装

	sudo add-apt-repository ppa:fcitx-team/nightly
	sudo add-apt-repository ppa:webupd8team/java
    sudo apt-add-repository ppa:svn/ppa
    sudo apt-add-repository ppa:richarvey/nodejs
    sudo apt-add-repository ppa:pi-rho/dev # vim74a
    sudo apt-add-repository ppa:git-core/ppa
    sudo apt-get update
	sudo apt-get install oracle-java7-installer fcitx-sogoupinyin
	sudo apt-get purge openjdk*
	
{r:java }手动安装jdk方法：<http://www.webupd8.org/2011/09/how-to-install-oracle-java-7-jdk-in.html>


### 手动安装java

1. 下载最新jdk  <http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html>
2. 解压jdk包，重命名解压目录为 **java-7-oracle**
3. 创建目录：`sudo mkdir -p  /usr/lib/jvm/`,将刚才创建的 java-7-oracle  /usr/lib/jvm/ 移动到这个目录
4. 下载并运行 [update-java](http://webupd8.googlecode.com/files/update-java-0.5b)  选择java-7-oracle

配置和安装必备软件
==========

设置输入法
-----------

> 安装好fcitx后，启动 `系统->fcitx配置工具`  配置好要使用的输入法

> Linux mint 14 还需要在 `系统->首选项->输入法切换器` 中将fcitx设置为当前区域的默认输入环境


配置桌面环境
-----------

> * 安装ubuntu tweak <http://ubuntu-tweak.com/>

> * 安装运行gconf-editor  配置12.04

> 	窗口最大最小化动画：`/desktop/gnome/interface/enable_animations`

> 	窗口按钮布局：`/apps/metacity/general/button_layout`

> 	混合器效果：`/apps/metacity/general/compositing_manager`

>	阴影特效：`/apps/metacity/general/compositor_effects`

> * 运行dconf  配置:12.10

>	窗口按钮布局：`org>gnome>desktop>wm>button-layout`

>	窗口最大最小化动画：`org>gnome>desktop>interface>enable-animations`

>	开混合器效果: `org>gnome>metacity>compositing-manager`

> * 运行mateconf-editor 配置Mint 14

>	`/apps/marco/general/compositing_manager`

>	`/apps/marco/general/button_layout`

>	`/desktop/mate/interface/enable_animations`


安装软件
----------

* 必备软件

		sudo apt-get -y install git-core chmsee gtkhash chromium-browser \
			gcolor2 gdebi audacious shutter ntfs-config synaptic system-config-samba amule filezilla acroread  meld gtk-recordmydesktop \
            nodejs npm subversion rubygems curl python-pip

	由于还会用到一些kde环境的软件，所以安装一些相关的kde组件(帮助中心和dolphin文件管理器)
	
		sudo apt-get -y install khelpcenter4  kate  dolphin
		
	电驴国内服务器列表:`ed2k://|serverlist|http://ed2k.im/server.met|/`

* 开发者工具

	sudo gem install sass 
    sudo npm install -g coffee-script
	sudo npm install less
	ln -s ~/node_modules/less/bin/lessc ~/bin/

* 推荐软件

webhttrack
:  网站整站下载抓取

freemind
:  思维导图软件，安装时可能会自动安装openjdk 6，设为默认javajdk

mypaint
: 类似Sketchbook pro的草绘工具

docky
: 很好用的dock软件

liferea
: RSS客户端

Dia
: UML绘制软件

calligraflow 
: 流程图软件

meld
: 文件比较合并工具

seahorse
: 密码和密钥

dbeaver
: 基于eclipse的数据库管理工具，java软件，解压后即可运行，不建议使用deb包。
支持 MySQL, PostgreSQL, Oracle, DB2, MSSQL, Sybase, Mimer, HSQLDB, Derby, 以及其他兼容 JDBC 的数据库 <http://dbeaver.jkiss.org/download/>

hwinfo
: 命令行的硬件信息查看工具

hardinfo
: 图形化的硬件信息查看工具
	

修改快捷键
---------

由于IDE中用到大量快捷键，所以应该把桌面环境的大多快捷键禁用，Fcitx的快捷键，调整为Alt+space切换输入法，Ctrl+C取消输入法

AMD显卡驱动安装
-------------

32位系统可以通过 “系统” 菜单中的 “附加驱动” 安装闭源显卡驱动，12.10以上附加驱动在 “软件源”的选项卡中;64位系统可能附加驱动中找不到闭源驱动，可以到<http://support.amd.com/us/Pages/AMDSupportHub.aspx>下载安装

### 版本说明：

12.06 版本适用于 HD2000,3000,4000显卡

13.3 版本适用于 HD5000,6000,7000系列显卡,支持 RH 5.7, 5.8, 6.2 ,6.3和 SUSE Linux Enterprise 10 SP4,11 SP2 和 OpenSUSE 11.4,12.1 和 Ubuntu 12.10

卸载程序位于：`/usr/share/ati`

### 去除beta版本的右下角水印

`sudo vi /etc/ati/signature`，替换为下面的签名：

`5da725748a98681ea0dd5ee8eeeff730:65891c4ca7a95a2e96ec6f89c3dec30169961537a7d93c5780ea66d8dddfc2:6b921d4dbbfb5c7dc1ea3fd1db8ec55564914017bcfe5027c3ee3a8d8ddcc7016b9e1c42bbad5b29c1ea3ad1dd8cc20765974015bcae0c26c4b93a8e8ddd`

设置文件模板
-----------

在 `~/模板` 目录下面创建一些常用的文件模板，它们可以在右键菜单的新建中出现


禁用ipv6
-----------

### 检测ipv6状态

 `ip a | grep inet6` 如果显示为空，表示以禁用

 `cat /proc/sys/net/ipv6/conf/all/disable_ipv6` 显示0说明ipv6开启，1说明关闭
	
### ubuntu 12.10关闭ipv6
 
在 /etc/sysctl.conf 增加下面几行，并重启。
 
	#disable IPv6
	net.ipv6.conf.all.disable_ipv6 = 1
	net.ipv6.conf.default.disable_ipv6 = 1
	net.ipv6.conf.lo.disable_ipv6 = 1
	

### 旧版本ubuntu
	
	在终端下输入      sudo vi /etc/modprobe.d/aliases
	注释掉这一行      alias net-pf-10 ipv6 
	存盘
	在终端下输入      sudo gedit /etc/modprobe.d/blacklist
	加入这一行        blacklist ipv6 
	
	必须重启电脑才生效。

	做完以上这些后，打开一个终端并输入：

	ip a | grep inet6
	如果没有任何输出就说明 ipv6 确实关闭了。
	
