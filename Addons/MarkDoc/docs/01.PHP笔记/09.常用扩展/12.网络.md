<p class="api">string gethostbyaddr(string $ip_address)</p>

Returns the host name of the Internet host specified by ip_address.

<p class="api">string gethostbyname(string $hostname)</p>

Returns the IPv4 address of the Internet host specified by hostname.

<p class="api">resource fsockopen(string $hostname[,int $port=-1[,int &$errno[,string &$errstr[,float $timeout=ini_get("default_socket_timeout")]]]])</p>

Initiates a socket connection to the resource specified by hostname.


<p class="api">array gethostbynamel(string $hostname)</p>

Returns a list of IPv4 addresses to which the Internet host specified by hostname resolves.

<p class="api">string gethostname(void)</p>

gets the standard host name for the local machine.

* checkdnsrr — Check DNS records corresponding to a given Internet host name or IP address
* closelog — Close connection to system logger
* define_syslog_variables — Initializes all syslog related variables
* dns_check_record — 别名 checkdnsrr
* dns_get_mx — 别名 getmxrr
* dns_get_record — Fetch DNS Resource Records associated with a hostname
* fsockopen — Open Internet or Unix domain socket connection
* gethostbyaddr — Get the Internet host name corresponding to a given IP address
* gethostbyname — Get the IPv4 address corresponding to a given Internet host name
* gethostbynamel — Get a list of IPv4 addresses corresponding to a given Internet host name
* gethostname — Gets the host name
* getmxrr — Get MX records corresponding to a given Internet host name
* getprotobyname — Get protocol number associated with protocol name
* getprotobynumber — Get protocol name associated with protocol number
* getservbyname — Get port number associated with an Internet service and protocol
* getservbyport — Get Internet service which corresponds to port and protocol
* header_register_callback — Call a header function
* header_remove — Remove previously set headers
* header — Send a raw HTTP header
* headers_list — Returns a list of response headers sent (or ready to send)
* headers_sent — Checks if or where headers have been sent
* http_response_code — Get or Set the HTTP response code
* inet_ntop — Converts a packed internet address to a human readable representation
* inet_pton — Converts a human readable IP address to its packed in_addr representation
* ip2long — Converts a string containing an (IPv4) Internet Protocol dotted address into a proper address
* long2ip — Converts an (IPv4) Internet network address into a string in Internet standard dotted format
* openlog — Open connection to system logger
* pfsockopen — Open persistent Internet or Unix domain socket connection
* setcookie — Send a cookie
* setrawcookie — Send a cookie without urlencoding the cookie value
* socket_get_status — 别名 stream_get_meta_data
* socket_set_blocking — 别名 stream_set_blocking
* socket_set_timeout — 别名 stream_set_timeout
* syslog — Generate a system log message