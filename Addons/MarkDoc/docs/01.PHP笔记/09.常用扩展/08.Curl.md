单线程用法
=========

初始化资源
----------

<p class="api">resource curl_init([string $url=NULL])</p>

初始化一个新的会话，返回一个cURL句柄，供 curl_setopt(), curl_exec()和 curl_close() 函数使用。

如果提供了$url参数，CURLOPT_URL 选项将会被设置成这个值。否则你必须使用 curl_setopt()函数手动地设置CURLOPT_URL的值。

<p class="api">resource curl_copy_handle(resource $ch)</p>

复制一个cURL句柄并保持相同的选项。

cURL参数设置
--------

<p class="api">bool curl_setopt  (resource $ch,int $option,mixed $value)</p>

<p class="api">bool curl_setopt_array (resource $ch,array $options)</p>

为给定的cURL会话句柄设置一个选项。

$ch ：由 curl_init() 返回的 cURL 句柄。
最常用的option

|配置项                |说明                                                                    |
|----------------------|------------------------------------------------------------------------|
|CURLOPT_RETURNTRANSFER|设为true会和返回内容而不是输出内容                                      |
|CURLOPT_POST          |设为ture可发送post数据                                                  |
|CURLOPT_POSTFIELDS    |要发送的post数据                                                        |
|CURLOPT_USERAGENT     |---                                                                     |
|CURLOPT_FILE          |设置一个文件资源，将返回内容保存到文件中                                |
|CURLOPT_HTTPHEADER    |发送的请求头信息，数组格式，每个类型是一个元素                          |
|CURLOPT_COOKIE        |发送的cookie字符串                                                      |
|CURLOPT_COOKIEJAR     |是否保存cookie                                                          |
|CURLOPT_HEADER        |是否输出响应头信息                                                      |
|CURLOPT_NOBODY        |不输出返回的响应数据（只输出响应头信息）                                |
|CURLOPT_PUT           |启用时允许HTTP发送文件，必须同时设置CURLOPT_INFILE和CURLOPT_INFILESIZE。|
|CURLOPT_UPLOAD        |启用后允许文件上传                                                      |



执行cURL
-----

<p class="api">mixed curl_exec(resource $ch)</p>

执行给定的cURL会话。

信息查询
---------

<p class="api">mixed curl_getinfo(resource $ch[,int $opt=0])</p>

返回最后一次访问的相关信息

$opt用于明确指定想要获取的信息类型，如果不设置则使用关联数组返回所有信息

$opt支持的参数：

- CURLINFO_EFFECTIVE_URL - 最后一个有效的URL地址
- CURLINFO_HTTP_CODE - 最后一个收到的HTTP代码
- CURLINFO_FILETIME - 远程获取文档的时间，如果无法获取，则返回值为“-1”
- CURLINFO_TOTAL_TIME - 最后一次传输所消耗的时间
- CURLINFO_NAMELOOKUP_TIME - 名称解析所消耗的时间
- CURLINFO_CONNECT_TIME - 建立连接所消耗的时间
- CURLINFO_PRETRANSFER_TIME - 从建立连接到准备传输所使用的时间
- CURLINFO_STARTTRANSFER_TIME - 从建立连接到传输开始所使用的时间
- CURLINFO_REDIRECT_TIME - 在事务传输开始前重定向所使用的时间
- CURLINFO_SIZE_UPLOAD - 上传数据量的总值
- CURLINFO_SIZE_DOWNLOAD - 下载数据量的总值
- CURLINFO_SPEED_DOWNLOAD - 平均下载速度
- CURLINFO_SPEED_UPLOAD - 平均上传速度
- CURLINFO_HEADER_SIZE - header部分的大小
- CURLINFO_HEADER_OUT - 发送请求的字符串
- CURLINFO_REQUEST_SIZE - 在HTTP请求中有问题的请求的大小
- CURLINFO_SSL_VERIFYRESULT - 通过设置CURLOPT_SSL_VERIFYPEER返回的SSL证书验证请求的结果
- CURLINFO_CONTENT_LENGTH_DOWNLOAD - 从Content-Length: field中读取的下载内容长度
- CURLINFO_CONTENT_LENGTH_UPLOAD - 上传内容大小的说明
- CURLINFO_CONTENT_TYPE - 下载内容的Content-Type:值，NULL表示服务器没有发送有效的Content-Type: header



<p class="api">string curl_error(resource $ch)</p>

返回一条最近一次cURL操作明确的文本的错误信息。返回错误信息或 ” (空字符串) 如果没有任何错误发生。

<p class="api">int curl_errno(resource $ch)</p>

返回最后一次cURL操作的错误号。返回错误号或 0 (零) 如果没有错误发生。

<p class="api">void curl_close(resource $ch)</p>

关闭一个cURL会话并且释放所有资源。cURL句柄ch 也会被释放。

并行curl
=========

初始化并行curl资源
-------------

<p class="api">resource curl_multi_init(void)</p>

并行地处理批处理cURL句柄。

添加curl资源句柄
--------------------------

<p class="api">int curl_multi_add_handle(resource $mh,resource $ch)</p>

增加 ch 句柄到批处理会话mh

mh:由 curl_multi_init() 返回的 cURL 多个句柄。

ch:由 curl_init() 返回的 cURL 句柄。

<p class="api">int curl_multi_remove_handle(resource $mh,resource $ch)</p>

从给定的批处理句柄mh中移除ch句柄。当ch句柄被移除以后，仍然可以合法地用 curl_exec()执行这个句柄。当正在移除的句柄正在被使用，在处理的过程中所有的传输任务会被终止。


执行并行curl
----------

<p class="api">int curl_multi_exec(resource $mh,int &$still_running)</p>

$still_running:一个用来判断操作是否仍在执行的标识的引用。大于0表示仍在执行过程中，等于0表示以执行结束

返回一个cURL码定义在cURL 预定义常量。该函数仅返回关于整个批处理栈相关的错误，返回CURLM_OK并不代表所有的线程都访问成功

* CURLM_CALL_MULTI_PERFORM (integer)  //正在执行中
* CURLM_OK (integer)  //所有线程执行完毕，一个或多个线程执行成功
* CURLM_BAD_HANDLE (integer)
* CURLM_BAD_EASY_HANDLE (integer)
* CURLM_OUT_OF_MEMORY (integer) //内存不足
* CURLM_INTERNAL_ERROR(integer) //内部错误



<p class="api"> array curl_multi_info_read(resource $mh[,int &$msgs_in_queue=NULL])</p>

在{y:**curl_multi_exec**执行过程中}返回正在执行的线程的信息，所有线程执行结束后再调用此函数返回false

$msgs_in_queue 仍在队列中的消息数量

成功时返回相关信息的数组，失败时返回FALSE。

#####返回数组的元素

msg
:   CURLMSG_DONE常量（即1）。其他返回值当前不可用。

result
:   CURLE_*  状态常量之一。如果一切操作没有问题，将会返回CURLE_OK常量(即0)。

handle
:   当前线程的cURL的资源句柄,最重要的元素


<p class="api">int curl_multi_select(resource $mh[,float $timeout=1.0])</p>

暂停一段时间，直到cURL批处理连接中的某一个连接得到了服务器响应。

成功时返回已执行的线程的数量。失败时返回-1，否则返回超时(从底层的select系统调用).


```php
$running=null;
do {
    curl_multi_exec($mh,$running);
    $ready=curl_multi_select($mh); // this will pause the loop
    if($ready>0){
        while($info=curl_multi_info_read($mh)){
            $status=curl_getinfo($info['handle'],CURLINFO_HTTP_CODE);
            if($status==200){
                $successUrl=curl_getinfo($info['handle'],CURLINFO_EFFECTIVE_URL);
                break 2;
            }
        }
    }
} while ($running>0 && $ready!=-1);
```


<p class="api"> string curl_multi_getcontent(resource $ch)</p>

如果CURLOPT_RETURNTRANSFER为true被设置到一个具体的句柄，那么这个函数将会以字符串的形式返回那个cURL句柄获取的内容。


<p class="api">void curl_multi_close(resource $mh)</p>

Closes a set of cURL handles.


```php
$urls = array ( "http://www.cnn.com/" , "http://www.bbc.co.uk/" , "http://www.yahoo.com/"
);
$mh = curl_multi_init();
foreach ($urls as $i => $url) {
	$conn[$i] = curl_init($url);
	curl_setopt($conn[$i], CURLOPT_RETURNTRANSFER, 1);
	curl_multi_add_handle($mh, $conn[$i]);
}
do {
	$status = curl_multi_exec($mh, $active);
	$info = curl_multi_info_read($mh);
	if (false !== $info) {
		var_dump($info);
	}
} while ($status === CURLM_CALL_MULTI_PERFORM || $active);
foreach ($urls as $i => $url) {
	$res[$i] = curl_multi_getcontent($conn[$i]);
	curl_close($conn[$i]);
}

var_dump(curl_multi_info_read($mh));
```

```php
function rolling_curl($urls, $delay) {
	$queue = curl_multi_init();
	$map = array();
	foreach ($urls as $url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_NOSIGNAL, true);
		curl_multi_add_handle($queue, $ch);
		$map[(string) $ch] = $url;
	}
	$responses = array();
	do {
		while (($code = curl_multi_exec($queue, $active)) == CURLM_CALL_MULTI_PERFORM) ;
		if ($code != CURLM_OK) {
			break; }
			// a request was just completed -- find out which one
			while ($done = curl_multi_info_read($queue)) {
				// get the info and content returned on the request
				$info  = curl_getinfo($done['handle']);
				$error = curl_error($done['handle']);
				$results = callback(curl_multi_getcontent($done['handle']), $delay);
				$responses[$map[(string) $done['handle']]] = compact('info', 'error', 'results');
				// remove the curl handle that just completed
				curl_multi_remove_handle($queue, $done['handle']);
				curl_close($done['handle']);
			}
			// Block for data in / output; error handling is done by curl_multi_exec
			if ($active > 0) {
				curl_multi_select($queue, 0.5);
			}
		} while ($active);
		curl_multi_close($queue);
		return $responses;
	}
```