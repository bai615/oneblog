统计 
======

<p class="api">
int count ( mixed $var [, int $mode = COUNT_NORMAL ] ) 
</p>

别名为sizeof(), 统计数组中元素的数量

默认只统计最外层，如果要递归统计，第二个参数设为1或COUNT_RECURSIVE常量。

* 返回值：

	* 如果变量为null，返回0；由于空数组肯定也返回是0，所以只有isset($var)检测为真且返回值为0才表示$var为空数组。

	* 如果$var不是数组或者实现了Conutable接口的对象，返回的是1

!!!如果安装了 SPL，还能用于Countable实例。返回值又其count方法决定。

```php
$food = array('fruits' => array('orange', 'banana', 'apple'),
              'veggie' => array('carrot', 'collard', 'pea'));

echo count($food, COUNT_RECURSIVE);
// 输出: 8

echo count($food); 
```


<p class="api">
array array_count_values ( array $input )
</p>

统计数组中{r:整型和字符串类型的元素}出现的次数，返回一个记录了统计结果的关联数组,key为元素$input元素的值,value是元素在$input中的数量。

!!!输入数组为空数组array()，则返回数组也为空数组array()

```php
$bar = array_count_values (array("    "));//数组元素是一个Tab空白
echo $bar["\t"];
//输出为1，也可以用echo $bar["    "]
```

计算 
=============
计算是数组元素reduce的具体简单实例,用数组迭代函数可以实现更复杂的元素计算

<p class="api">
mixed array_product($array) 
</p>

把数组中的数值进行相乘，返回乘积值（整型或浮点型）

字符串和布尔型会还有null自动转换为数值型。数组和对象元素会被忽略。

<p class="api">
mixed array_sum($array) 
</p>

把数组中的数值进行相加，返回相加值（整型或浮点型） 

字符串和布尔型会还有null自动转换为数值型。数组和对象元素会被忽略。
 
提取元素 
========

## 随机提取

<p class="api">
mixed array_rand ( array $input [, int $num_req = 1 ] )
</p>

从数组中随机选出{g:一个元素，并返回该元素的键名；}

从数组中随机选出{g:多个元素，并返回这些元素的{b:键名所组成的索引数组}}

## 选择元素

<p class="api">
array array_slice ( array $array , int $offset [, int $length = NULL [, bool $preserve_keys = false ]] )
</p>

从数组中选择元素复制后返回一个新数组

preserve_keys	复制时是否保留原来的key,默认为false,不保留。
 
如果起始位置为负，偏移量超过了数组的第一个元素，指针被自动定义为0。

其他任何无法产生有效范围的参数，都会返回空数组。{r:任何情况不会返回false，所以进行真假判断是不能使用 === }

PS:字符串函数的相似函数：<span class="api">string|false substr ( string $string , int $start [, int $length ] )</span>

## 提取首尾元素

<p class="api">
mixed array_pop ( array &$array )
</p>

提取最后一个元素的value，{r:输入数组中将失去这个元素，且输入数组的指针将被reset}

如果是空数组或者不是数组，返回null

<p class="api">
mixed array_shift ( array &$array )
</p>

提取第一个元素的value，{r:输入数组中将失去这个元素，且输入数组的指针将被reset}

如果是空数组或者不是数组，返回null

<p class="api">
mixed end ( array &$array )
</p>

提取最后一个元素的value，{r:并将输入数组的指针移动到最后一个元素上，而不是末尾}

如果是空数组，返回false。如果不是数组，返回NULL

```php
$fruits = array('apple', 'banana', 'cranberry');
echo end($fruits); // cranberry
echo current($fruits);// cranberry
```

<p class="api">
mixed reset ( array &$array )
</p>

提取第一个元素的value，{r:并将输入数组的指针移动到低一个元素上}

如果是空数组，返回false。如果不是数组，返回NULL
