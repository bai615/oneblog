
<p class="api">
array array_merge ( array $array1 [, array $array2 [, array $... ]] )
</p>

函数把两个或多个数组的元素合并为一个数组.

合并规则1：数值索引元素都全部保留，在合并后会会重建索引号，哪怕只提供了一个数组参数。

合并规则2：如果字符串键名重复，后面的值会覆盖前面的。

<p class="api">
array array_merge_recursive ( array $array1 [, array $... ] )
</p>

合并规则1：数值索引元素都全部保留，在合并后会会重建索引号，如果只提供了一个数组参数则只对最上层元素重建索引，而不会递归重建索引。

合并规则2：键名相同且一个元素的值为数组，则其他元素的值也会转换为数组后执行merge。




<p class="api">
array|false  array_combine ( array $keys , array $values )
</p>

创建一个数组，用一个数组的值作为其键名，另一个数组的值作为其值

!!!!!如果两个数组的单元数不同或者数组为空时返回 FALSE。 