 
包含了来自web服务器，客户端，运行环境和用户输入的数据，在当前脚本的任何地方都都可以直接使用这些数据

1. $_GET   经过url提交至脚本的变量 。字节有限，url明文传递
2. $_REQUEST  尽量不使用，因为它可以接收任何数据
3. $_POST  经过 http post method提交至脚本的变量
4. $_FILES 经过 http post method 文件上传提交至脚本的变量
5. $_COOKIE
6. $_SESSION
7. $_ENV    执行环境提交至脚本的变量
8. $_SERVER  web服务器设定的，或直接和当前脚本的执行


常用的

1. $_SERVER["HTTP_HOST"]  当前请求头中 Host: 项的内容，如果存在的话。
2. $_SERVER["HTTP_ACCEPT_LANGUAGE"]  访客浏览器语言
3. $_SERVER["HTTP_ACCEPT_CHARSET"]  访客使用的字符集
4. $_SERVER["HTTP_REFERER"]  从哪个网页访问本页
5. $_SERVER["REMOTE_ADDR"]  访客ip
6. $_SERVER["DOCUMENT_ROOT"] 即网站的根目录路径例如：/home/www/
7. $_SERVER["SERVER_ADDR"]  服务器ip
8. $_SERVER["SERVER_NAME"]  当前运行脚本所在的服务器的主机名。如果脚本运行于虚拟主机中，该名称是由那个虚拟主机所设置的值决定。
9. $_SERVER['REQUEST_TIME'] 请求发生时的时间戳
10. $_SERVER['HTTP_X_REQUESTED_WITH'] 这是JQuery的ajax头信息。如果值为：xmlhttprequest，表示为ajax请求

urlmanger组件常用的

* $_SERVER["SCRIPT_NAME"] 脚本在域上面的绝对路径，例如：/index.php
* $_SERVER["QUERY_STRING"] 例如：act=phpinfo
* $_SERVER["REQUEST_URI"]  例如：/index.php?act=phpinfo
* $_SERVER["SCRIPT_FILENAME"]  脚本在文件系统上的绝对路径，例如：/home/www/bbs/index.php
* 凡是文件和路径的值都是 "/"开头
* $_SERVER["REQUEST_URI"]    =$_SERVER["SCRIPT_NAME"] . '?' . $_SERVER["QUERY_STRING"];
* $_SERVER["SCRIPT_FILENAME"]=$_SERVER["DOCUMENT_ROOT"]  .  $_SERVER["SCRIPT_NAME"];

ThinkPHP的pathinfo模式的$_SERVER变量

```php
  'DOCUMENT_ROOT' =>string'/home/zhuyajie/Dropbox/'(length=23)
  'SCRIPT_NAME' =>string'/test/index.php'(length=15)
  'SCRIPT_FILENAME' =>string'/home/zhuyajie/Dropbox/test/index.php'(length=37)
  'QUERY_STRING' =>string''(length=0)
  'REQUEST_URI' =>string'/test/index.php/index/addUser'(length=29)
  'PATH_INFO' =>string'/index/addUser'(length=14)
  'PATH_TRANSLATED' =>string'/home/zhuyajie/Dropbox/index/addUser'(length=36)
  'PHP_SELF' =>string'/test/index.php/index/addUser'(length=29)
```

ThinkPHP的普通模式的$_SERVER

```php

  'DOCUMENT_ROOT' =>string'/home/zhuyajie/Dropbox/'(length=23)
  'SCRIPT_FILENAME' =>string'/home/zhuyajie/Dropbox/test/index.php'(length=37)
  'SCRIPT_NAME' =>string'/test/index.php'(length=15)
  'PHP_SELF' =>string'/test/index.php'(length=15)
  'QUERY_STRING' =>string'm=index&a=addUser'(length=17)
  'REQUEST_URI' =>string'/test/index.php?m=index&a=addUser'(length=33)
```