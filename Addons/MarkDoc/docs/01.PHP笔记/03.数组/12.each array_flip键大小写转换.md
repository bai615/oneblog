<p class="api">
array|false each ( array &$array) 
</p>

将数组$array中指针位置的元素分解成一个数组返回，并将$array的指针移动到下一个元素上。

使用完这个函数后，最好执行reset()对$array指针复位

分解得到的数组结构如下：

```php
array(
    [0]=键名
    [1]=值
    [key]=键名
    [value]=值
)
```

这个结果可以用list()将元素的key和value赋值给两个变量。

当到返回false时，表示指针已经到达数组的末尾，相比next()，each的结果是可靠的。

<p class="api">
array array_flip ( array $trans )
</p>

交换数组中的键和值,返回新数组 

注意 $trans 中的value应该是标量，以在互换后能作为合法的键名；如果值的类型不对将发出一个警告，并且有问题的键／值对将不会反转。

如果同一个值出现了多次，则最后一个键名将作为它的值，所有其它的都丢失了。


<p class="api">
array array_change_key_case ( array $input [, int $case ] ) 
</p>

更改数组键名的大小写，返回新数组；

$case有两个可选值 CASE_UPPER 和 CASE_LOWER,默认值是 CASE_LOWER 

注意：改变大小写，有可能使得两个原本不同的键名变成冲突的键名，这时只保留最后一个元素。