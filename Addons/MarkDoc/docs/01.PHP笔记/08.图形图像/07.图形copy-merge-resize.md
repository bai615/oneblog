<p class="api">bool imagealphablending ( resource $image , bool $blendmode )</p>

<p class="api">bool imagecopy (resource $dst_im,resource $src_im,int $dst_x,int $dst_y,int $src_x,int $src_y,int $src_w,int $src_h )</p>

将 src_im 图像中坐标从 src_x，src_y 开始，宽度为 src_w，高度为 src_h的一部分拷贝到 dst_im 图像中坐标为 dst_x 和 dst_y 的位置上。

默认为像素替换模式。如果复制含有透明像素的图片，要设置混合模式：imagealphablending($im, true);

<p class="api">bool imagecopymerge (resource $dst_im,resource $src_im,int $dst_x,int $dst_y,int $src_x,int $src_y,int $src_w,int $src_h,int $pct)</p>

与imagecopy()的不同之处在于，该函数可以设置被复制的图像的透明度(原有的像素中的alpha信息会先被丢弃)，然后与目标图像混合。

pct值范围从 0 到 100。当 pct = 0 时，实际上什么也没做(src完全透明)，当为 100 时对于调色板图像本函数和 imagecopy() 完全一样。 

注意：此函数拷贝时，原来图像的透明度信息丢失。所以不应该用于含有透明像素的图片。

`imagecopymergegray`  函数和 imagecopymerge()唯一不同在于它保留了src_im的色相（通过将dst_im被覆盖的区域转为灰度实现）。 

<p class="api">bool imagecopyresized (resource $dst_image,resource $src_image,int $dst_x,int $dst_y,int $src_x,int $src_y,int $dst_w,int $dst_h,int $src_w,int $src_h)</p>

将$src图像中的一块矩形区域拷贝到$dst图像中。如果$src源和目标$dst的宽度和高度不同，则$src会进行相应的图像收缩和拉伸。

坐标原点指的是左上角。

本函数在缩放图像的时候，不会进行插值计算，也就是低质量缩放，文件尺寸也比较大，唯一优点速度比较快

<p class="api">bool imagecopyresampled ( resource $dst_image,resource $src_image,int $dst_x,int $dst_y,int $src_x,int $src_y,int $dst_w,int $dst_h,int $src_w,int $src_h)</p>

该函数内部采用正弦插值算法（质量最优的插值算法）和程度为1的模糊算法进行插值计算，非常耗时耗资源。

效果等同于 imagick 的 $img->resizeImage ( $w, $h ,imagick::FILTER_SINC, 1 ,true ); 但同样效果 imagick的耗时仅进为GD的1／3


####!!GD2与imagick和gmagick的Resize性能对比

`imagick::resizeImage ( $w, $h ,imagick::FILTER_SINC, 0 ,true )`有更好的插值效果，但图像体积只有GD的不到1／2，同时保留图片的exif信息

imagick::thumbnailImage生成的图像体积更小一些，运算耗时也多一些。

`gmagick::resizeImage ( $w, $h ,imagick::FILTER_SINC, 0 ,true )`会删除exif信息，图像质量也比imagick明显低（gmagick的默认压缩质量是75，imagick默认为96）

可使用gmagick::setCompressionQuality(100) ;设置压缩质量，推荐值90

