 声明元素和属性
=================

元素和属性
----------

> xs:element

> xs:attribute

### name 和 type属性
	
name属性声明元素或属性名称，type属性（可选）声明元素或属性支持的数据类型

	<xs:element name="xxx" type="yyy"/>

### default 和 fixed属性

声明元素或属性的的默认值和固定值

	<xs:element name="color" fixed="red"/>
	<xs:element name="color" default="red"/>
	

限定元素或属性
=================

xs:simpleType
-----------

> 定义一个简单类型，规定约束以及关于属性或仅含文本的元素的值的信息


* 出现次数        无限制
* 支持的父元素    attribute、element、list、restriction (simpleType)、schema、union
* 支持的子元素    annotation、list、restriction (simpleType)、union

属性：

* id     可选。规定该元素的唯一的 ID。
* name    类型名称。 该名称必须是在 XML 命名空间规范中定义的无冒号名称 (NCName)。 如果指定，该名称在所有 simpleType 和 complexType 元素之间必须是唯一的。 如果 simpleType 元素是 schema 元素的子元素，则为必选项，在其他时候则是不允许的。

xs:complexType
--------------

> 定义复杂类型

* 父元素	element、redefine、schema
* 内容	(annotation?,(simpleContent|complexContent|((group|all|choice|sequence)?,((attribute|attributeGroup)*,anyAttribute?))))


### 属性

* id 可选。规定该元素的唯一的 ID。

* name 可选。规定元素的名称。

* abstract 可选。规定在实例文档中是否可以使用复杂类型。如果该值为 true，则元素不能直接使用该复杂类型，而是必须使用从该复杂类型派生的复杂类型。 默认值为 false。

* mixed 可选。规定是否允许字符数据出现在该复杂类型的子元素之间。 默认值为 false。

	* 如果 simpleContent 元素是子元素，则不允许 mixed 属性。
	* 如果 complexContent 元素是子元素，则该 mixed 属性可被 complexContent 元素的 mixed 属性重写。
	
* block 可选。防止具有指定派生类型的复杂类型被用来替代该复杂类型。该值可以包含 #all 或者一个列表，该列表是 extension 或 restriction 的子集：

	* extension - 防止通过扩展派生的复杂类型被用来替代该复杂类型。
	* restriction - 防止通过限制派生的复杂类型被用来替代该复杂类型。
	* #all - 防止所有派生的复杂类型被用来替代该复杂类型。
	
* final 可选。防止从该 complexType 元素派生指定的类型。该值可以包含 #all 或者一个列表，该列表是 extension 或 restriction 的子集。

	* extension - 防止通过扩展派生。
	* restriction - 防止通过限制派生。
	* #all - 防止所有派生（扩展和限制）。
	

xs:extension
-----------

> 对 simpleType 或 complexType 的元素进行扩展。

* 出现次数	一次
* 父元素	complexContent 、 simpleContent
* 内容	annotation、attribute、attributeGroup、anyAttribute、choice、all、sequence、group

xs:simpleContent
--------------

> 包含对 complexType 元素（它以字符数据或 simpleType 元素为内容）的扩展或限制并且不包含任何元素。

* 出现次数	一次
* 父元素	complexType
* 内容	

xs:complexContent
-----------------

> 包含对 complexType 元素（它以字符数据或 simpleType 元素为内容）的扩展或限制并且不包含任何元素。

* 出现次数	一次
* 父元素	complexType
* 内容	必选项 — 有并且只有一个下列元素： restriction (simpleContent) 或 extension (simpleContent)。必选项 — 有并且只有一个下列元素： restriction (simpleContent) 或 extension (simpleContent)。



xs:restriction
------------

> 定义对 simpleType、simpleContent 或 complexContent 定义的约束。

父元素	complexContent 、simpleType、simpleContent

内容	group、all、choice、sequence、attribute、attributeGroup、anyAttribute

语法

	<restriction
	id=ID
	base=QName
	any attributes
	>

	Content for simpleType:
	(annotation?,(simpleType?,(minExclusive|minInclusive| 
	maxExclusive|maxInclusive|totalDigits|fractionDigits|
	length|minLength|maxLength|enumeration|whiteSpace|pattern)*))

	Content for simpleContent:
	(annotation?,(simpleType?,(minExclusive |minInclusive| 
	maxExclusive|maxInclusive|totalDigits|fractionDigits|
	length|minLength|maxLength|enumeration|whiteSpace|pattern)*)?, 
	((attribute|attributeGroup)*,anyAttribute?))

	Content for complexContent:
	(annotation?,(group|all|choice|sequence)?,
	((attribute|attributeGroup)*,anyAttribute?))

	</restriction>
	
用于限定的元素:

* **enumeration**	定义可接受值的一个列表
* **fractionDigits**	定义所允许的最大的小数位数。必须大于等于0。
* **length**	定义所允许的字符或者列表项目的精确数目。必须大于或等于0。
* **maxExclusive**	定义数值的上限。所允许的值必须小于此值。
* **maxInclusive**	定义数值的上限。所允许的值必须小于或等于此值。
* **maxLength**	定义所允许的字符或者列表项目的最大数目。必须大于或等于0。
* **minExclusive**	定义数值的下限。所允许的值必需大于此值。
* **minInclusive**	定义数值的下限。所允许的值必需大于或等于此值。
* **minLength**	定义所允许的字符或者列表项目的最小数目。必须大于或等于0。
* **pattern**	定义可接受的字符的精确序列。
* **totalDigits**	定义所允许的阿拉伯数字的精确位数。必须大于0。
* **whiteSpace**	定义空白字符（换行、回车、空格以及制表符）的处理方式。



```
<xs:element name="age">
	<xs:simpleType>
	<xs:restriction base="xs:integer">
		<xs:minInclusive value="0"/>
		<xs:maxInclusive value="120"/>
	</xs:restriction>
	</xs:simpleType>
</xs:element> 
```


指示器
======

> 有七种指示器

Order 指示器
------------

属性：

* maxOccurs	可选。元素可出现的最大次数。 该值必须是 1。
* minOccurs	可选。元素可出现的最小次数。 该值可以是整数 0 或 1。若要指定该元素是可选的，请将该属性设置为 0。 默认值为 1。

### xs:all

> 指示器规定子元素可以按照任意顺序出现，且每个子元素必须只出现一次


* 出现次数	一次
* 父元素	group、restriction (simpleContent)、extension (simpleContent)、restriction (complexContent)、extension (complexContent)、complexType
* 内容	annotation、element

### xs:choice

> 指示器规定可出现某个子元素或者可出现另外一个子元素（非此即彼）

* 出现次数	在 group 和 complexType 元素中为一次；其他为无限制。
* 父元素	group、choice、sequence、complexType、restriction (simpleContent)、extension (simpleContent)、restriction (complexContent)、extension (complexContent)
* 内容	annotation、any、choice、element、group、sequence

### xs:sequence 

> 元素要求组中的元素以指定的顺序出现在包含元素中。每个子元素可出现 0 次到任意次数。

内容：annotation、any、choice、element、group、sequence

属性

* id	可选。规定该元素的唯一的 ID。
* maxOccurs	可选。规定 any 元素在父元素中可出现的最大次数。该值可以是大于或等于零的整数。若不想对最大次数设置任何限制，请使用字符串 "unbounded"。 默认值为 1。
* minOccurs	可选。规定 any 元素在父元素中可出现的最小次数。该值可以是大于或等于零的整数。若要指定该 any 组是可选的，请将此属性设置为零。 默认值为 1。

Occurrence 指示器属性
---------------

> Occurrence 属性 指示器用于定义某个元素出现的频率。

* maxOccurs 属性

* minOccurs 属性


Group 指示器
-----------

### attributeGroup

用于对属性声明进行组合，这样这些声明就能够以组合的形式合并到复杂类型中。


* 父元素	attributeGroup、complexType、schema、restriction (simpleContent)、extension (simpleContent)、restriction (complexContent)、extension (complexContent)
* 内容	annotation、attribute、attributeGroup、anyAttribute

属性

* name	可选。规定属性组的名称。name 和 ref 属性不能同时出现。
* ref	可选。规定对指定的属性组的引用。name 和 ref 属性不能同时出现

### group

* name 可选。规定组的名称。该名称必须是在 XML 命名空间规范中定义的无冒号名称 (NCName)。 仅当 schema 元素是该 group 元素的父元素时才使用该属性。在此情况下，group 是由 complexType、choice 和 sequence 元素使用的模型组。 name 属性和 ref 属性不能同时出现。
* ref 可选。引用另一个组的名称。ref 值必须是 QName。 ref 可以包含命名空间前缀。 name 属性和 ref 属性不能同时出现。
* maxOccurs 可选。规定 group 元素可在父元素中出现的最大次数。该值可以是大于或等于零的整数。若不想对最大次数设置任何限制，请使用字符串 "unbounded"。默认值为 1。
* minOccurs 可选。规定 group 元素可在父元素中出现的最小次数。该值可以是大于或等于零的整数。默认值为 1。roup name




数据类型
========

字符串
---------

### xs:string

字符串数据类型可包含字符、换行、回车以及制表符。

### xs:normalizedString

规格化字符串数据类型同样可包含字符，但是 XML 处理器会移除折行，回车以及制表符。

### xs:token

Token 数据类型同样可包含字符，但是 XML 处理器会移除换行符、回车、制表符、开头和结尾的空格以及（连续的）空格。

### 对字符串数据类型的限定属性

* enumeration
* length
* maxLength
* minLength
* pattern (NMTOKENS、IDREFS 以及 ENTITIES 无法使用此约束)
* whiteSpace

数值
------

### xs:integer

整型

### 更多数值类型

* xs:byte	有正负的 8 位整数
* xs:decimal	十进制数
* xs:int	有正负的 32 位整数
* xs:integer	整数值
* xs:long	有正负的 64 位整数
* xs:negativeInteger	仅包含负值的整数 ( .., -2, -1.)
* xs:nonNegativeInteger	仅包含非负值的整数 (0, 1, 2, ..)
* xs:nonPositiveInteger	仅包含非正值的整数 (.., -2, -1, 0)
* xs:positiveInteger	仅包含正值的整数 (1, 2, ..)
* xs:short	有正负的 16 位整数
* xs:unsignedLong	无正负的 64 位整数
* xs:unsignedInt	无正负的 32 位整数
* xs:unsignedShort	无正负的 16 位整数
* xs:unsignedByte	无正负的 8 位整数


### 对数值数据类型的限定属性

* enumeration
* fractionDigits
* maxExclusive
* maxInclusive
* minExclusive
* minInclusive
* pattern
* totalDigits
* whiteSpace


其他数据类型
---------

> 时间日期型，布尔型，十六进制型，base64Binary，URI型

* xs:boolean
* xs:hexBinary
* xs:base64Binary
* xs:anyURI

可与杂项数据类型一同使用的限定：

* enumeration (布尔数据类型无法使用此约束*)
* length (布尔数据类型无法使用此约束)
* maxLength (布尔数据类型无法使用此约束)
* minLength (布尔数据类型无法使用此约束)
* pattern
* whiteSpace