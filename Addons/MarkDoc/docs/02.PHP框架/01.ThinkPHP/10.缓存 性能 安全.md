
缓存
=====

动态缓存
--------

> `$Cache = Cache::getInstance('缓存方式','缓存参数'),`


|{thw:4em}@                                                 |@               |@                                                                                                                                                                                                         |
|---------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|缓存方式                                           |{col:2}可以支持 File、Apachenote、Apc、Eaccelerator、Memcache、Shmop、Sqlite、Db、Redis和Xcache                                                                                                                            |
|{row:12}缓存参数<br>(根据不同的缓存方式存在不同的参数)       |通用缓存参数    |expire 缓存有效期（默认由DATA_CACHE_TIME参数配置） <br>length 缓存队列长度（默认为0）<br>queue 缓存队列方式（默认为file 还支持xcache和apc）                                                               |
                                                 |缓存方式         |额外支持的缓存参数                                                                                                                                                                                        |
                                                |File（文件缓存）  |temp 缓存目录（默认由DATA_CACHE_PATH参数配置）                                                                                                                                                            |
                                                 |Apachenote缓存  |host 缓存服务器地址（ 默认为127.0.0.1）                                                                                                                                                                   |
                                                 |Apc缓存         |暂无其他参数                                                                                                                                                                                              |
                                                 |Eaccelerator缓存|暂无其他参数                                                                                                                                                                                              |
                                                 |Xcache缓存      |暂无其他参数                                                                                                                                                                                              |
                                                 |Memcache        |host 缓存服务器地址（ 默认为127.0.0.1） <br> port 端口（默认为MEMCACHE_PORT参数或者11211） <br> timeout 缓存超时（默认由DATA_CACHE_TIME参数设置） <br> persistent 长连接（默认为false）                   |
                                                 |Shmop           |size（默认由SHARE_MEM_SIZE参数设置） <br> tmp（默认为TEMP_PATH） <br> project （默认为s） <br> length 缓存队列长度（默认为0）                                                                             |
                                                 |Sqlite          |db 数据库名称（默认:memory:） <br> table 表名（默认为sharedmemory） <br> persistent 长连接（默认为false）                                                                                                 |
                                                 |Db              |db 数据库名称（默认由DB_NAME参数配置） <br> table 数据表名称（默认由DATA_CACHE_TABLE参数配置）                                                                                                            |
                                                 |Redis           |host 服务器地址（默认由REDIS_HOST参数配置或者127.0.0.1）<br> port端口（默认由REDIS_PORT参数配置或者6379） <br> timeout 超时时间（默认由DATA_CACHE_TIME配置或者false） <br> persistent长连接（默认为false）|


	$Cache = Cache::getInstance('Xcache',array('expire'=>'60')),
	$Cache->setOptions('length',10);

	$Cache->name = 'ThinkPHP';
	$value = $Cache->name;
	Unset($Cache->name);
	$Cache->set('name','ThinkPHP',3600);  // 缓存name数据3600秒

快捷缓存
-------

	//过程式：
	cache(array('type'=>'xcache','expire'=>60));//初始化
	cache('a',$value);//写入缓存
	$value = cache('a');//读取缓存
	cache('a',null);//删除缓存
	//面向对象式
	$cache = cache(array('type'=>'xcache','expire'=>60));
	$cache->name = $value;
	echo $cache->name;
	unset($cache->name);

默认缓存设置
----

	'DATA_CACHE_TYPE'=>'Xcache',
	'DATA_CACHE_TIME'=>3600,

文件缓存设置
---------

	'DATA_CACHE_SUBDIR'=>true
	'DATA_PATH_LEVEL'=>2

查询缓存
---------

模型类的 `cache($key=true,$expire=null,$type='')`方法可以实现对查询结果的缓存，如果存在缓存直接返回缓存，如果不存在则执行查询。

	$Model->cache(true,60,'xcache')->select();

SQL解析缓存
--------

对生成的查询类sql语句进行缓存

	'DB_SQL_BUILD_CACHE' => true,
	'DB_SQL_BUILD_QUEUE' => 'xcache',
	'DB_SQL_BUILD_LENGTH' => 20, // SQL缓存的队列长度

性能
=======

关闭调试模式，开启缓存，手动设置模型字段$fields属性，替换入口

安全
========