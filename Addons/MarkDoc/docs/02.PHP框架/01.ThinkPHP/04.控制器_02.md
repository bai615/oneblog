获取URL参数
===========

> 直接根据url字符串获取请求参数

> 默认配置: ` 'VAR_URL_PARAMS' => '_URL_', // PATHINFO URL参数变量`

例如,对于url:`http://serverName/News/archive/2012/01/15`

在控制器中可以通过下面的的方法获取url中的参数:

```
    $year    = $_GET["_URL_"][2]; //'2012'
    $month  = $_GET["_URL_"][3];  //'01'
    $day    = $_GET["_URL_"][4];  //'15'
    $this->_param(2); //'2012'
    $this->_param(3); //'01'
```

!!!!!如果 VAR_URL_PARAMS设为空,则不能使用以上方法获取参数


前置后置操作
============

> 系统会检测当前操作是否具有前置和后置操作，如果存在就会按照顺序执行，
前置和后置操作的方法名是在要执行的方法前面加 `_before_` 和 `_after_`

例如: public function _before_index() 会在 index操作执行之前调用


Action参数绑定
==============

> Action参数绑定的原理是把URL中的参数（不包括分组、模块和操作地址）和控制器的操作方法中定义的参数进行绑定。

例如:

```
 public function read($id=0){
        echo 'id='.$id;
        $Blog = M('Blog');
        $Blog->find($id);
    }
```

当访问read操作时,参数$id会从url的id参数取值

多层控制器支持
==============

> 3.1版本开始，控制器支持自定义分层。同时A方法增加第二个参数layer，用于设置控制器分层。

例如 : `A('User','Event');` 表示实例化 Lib/Event/UserEvent.class.php。

分层控制器仅用于{y:在控制器内部调用,通过URL访问的控制器还是Action层}，
但是可以配置 DEFAULT_C_LAYER修改默认控制器层名称（该参数默认值为Action）。


页面重定向
===========

> 重定向是通过发送header来实现，所以使用之前不能有任何页面输出

使用U函数得到一个有效的url以后传给 `redirect($url, $time=0, $msg='')` 函数 即可完成页面重定向，函数中的$url参数是一个有效的url

也可以使用Aciton对象中的 redirect($url, $param=array(), $time=0, $msg='')方法，它会在内部调用U函数将$url和$param参数组装成有效url后调用redirect函数

__call方法
============

TP的 `__call`方法内封装了实现超全局数据过滤和请求类型检测功能的伪方法：

请求类型检测伪方法
-----------------

**isget(),ispost(),isput(),isdelete(),ishead()**（通过判断$_SERVER['REQUEST_METHOD']实现）

超全局数组过滤
-----------

> 默认使用htmlspecialchars()函数过滤要读取的值，可以在第二个参数制定其他过滤函数或在配置文件中配置默认的过滤函数：'DEFAULT_FILTER'

`_get($name), _post($name), _globals($name), _session($name), _cookie($name), _put($name), _server($name)`


__set,__get方法
=========

> 可以通过访问属性的方式为模板分配变量或者读取模板变量，建议使用assign方法分配模板变量,提高性能

    public function __set($name,$value) {
        $this->assign($name,$value);
    }

    

3.1增强
=======


可以设置多个过滤函数，依次执行过滤：'DEFAULT_FILTER'=>'htmlspecialchars,strip_tags'  ，_get('id','htmlspecialchars,strip_tags');

可以不执行过滤，超全局数组过滤方法的第二个参数设为false或''

ajaxReturn
----------

返回ajax数据给客户端：Action::ajaxReturn($data,$type='')

该方法之前不能有任何浏览器输出，它属于根据返回的$type发送header

默认为json数据，还支持jsonp,xml,eval数据。更改默认返回数据类型通过配置：'DEFAULT_AJAX_RETURN'

_initialize()
-------------

_initialize方法会在所有操作方法调用之前首先执行


支持设置多种伪静态
-------------

`'URL_HTML_SUFFIX'=>'html|shmtl|xml'`，U函数在生成url的时候，使用第一种伪静态。使用任一种设定的伪静态后缀都可以正常访问。

其他
--------

每个分组都可以定义自己的空模块类EmptyAction

cookie方法增加对数组的支持

get_client_ip函数添加$type参数，传入任何非零参数返回整型IP，方便IP定位比较，默认值 0。

R方法增加了第三个参数layer，用于设置控制器的分层名称

改进数据库session驱动，增加对系统session配置参数的支持。

U函数支持默认值，默认为空，获取当前地址

改进系统模板标签对session和cookie前缀的支持

改进系统标签对二级配置的支持

F函数支持子目录递归创建功能 F('sub1/sub2/name','value'); 会在DATA_PATH目录下面自动创建sub1/sub2 子目录，然后生成name缓存。

Action类的魔术方法中对操作方法的判断改进 支持 C('ACTION_SUFFIX') 后缀设置

Action类的show方法输出的内容可以支持模板解析了，便于数据库存取模板内容操作了
 
