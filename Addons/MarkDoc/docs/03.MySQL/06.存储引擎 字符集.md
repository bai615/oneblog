存储引擎
=========

可以针对不同的存储需求选择最优的存储引擎 

|存储引擎 |声明语法                                |特点                                    |
|---------|----------------------------------------|----------------------------------------|
|MyISAM   |create\|alter table [...] ENGINE=MyISAM;|读取速度快，支持全文索引                |
|InnoDB   |create\|alter table [...] ENGINE=InnoDB;|支持事务处理、故障恢复、行锁定、外键约束|


数据备份
----------

myisam以目录和文件的方式保存数据，每张表是一个数据文件，可以通过复制目录的方式备份数据库；

!!!!!Innodb则使用.idb文件保存数据，这个文件在数据库运行时一直不断更新，所以不能通过复制这个文件备份数据库，容易引起数据毁灭。必须使用mysqldump备份数据库。

如果InnoDB表在创建时，使用了.ibd文件中的自己的表空间，则这样的文件可以被删除和导入：

```
# 删除 
ALTER TABLE tbl_name DISCARD TABLESPACE;
# 导入 把备份的.ibd文件复制到数据库目录中，然后书写此语句：
ALTER TABLE tbl_name IMPORT TABLESPACE;
```

innodb事务
-----------

多条sql语句全部都执行成功后，结果符合期望，才进行提交，否则回滚到事务执行前的状态进行修改。

!!!!!innoDB默认为自动提交(autocommit) ,需要执行事务处理时，首先关闭自动提交，事务处理结束后，记得打开自动提交，否则后面的语句都会被一直当作事务。

* 关闭自动提交：`set autocommit=0;`
* 开启事务：`start transaction;`
* 回滚：`roolback;`
* 提交：`commit;`  提交表示语句执行符合预期，无需回滚修改，提交后便不能再回滚。

InnoDB 还是 MyISAM?
====================

MyISAM 是MySQL5.1默认的存储引擎，一般来说不是有太多人关心这个东西。

下面先让我们回答一些问题：

* 你的数据库有外键吗？
* 你需要事务支持吗？
* 你需要全文索引吗？
* 你经常使用什么样的查询模式？
* 你的数据有多大？

思考上面这些问题可以让你找到合适的方向，但那并不是绝对的。

如果你需要事务处理或是外键，那么InnoDB 可能是比较好的方式。如果你需要全文索引，那么通常来说 MyISAM是好的选择，因为这是系统内建的，然而，我们其实并不会经常地去测试两百万行记录。所以，就算是慢一点，我们可以通过使用Sphinx从InnoDB中获得全文索引。

数据的大小，是一个影响你选择什么样存储引擎的重要因素，大尺寸的数据集趋向于选择InnoDB方式，因为其支持事务处理和故障恢复。数据库的大小决定了故障恢复的时间长短，InnoDB可以利用事务日志进行数据恢复，这会比较快。而MyISAM可能会需要几个小时甚至几天来干这些事，InnoDB只需要几分钟。

您操作数据库表的习惯可能也会是一个对性能影响很大的因素。比如： COUNT() 在 MyISAM 表中会非常快，而在InnoDB 表下可能会很痛苦。

而主键查询则在InnoDB下会相当相当的快，但需要小心的是如果我们的主键太长了也会导致性能问题。大批的 inserts 语句在MyISAM下会快一些，但是 updates 在InnoDB 下会更快一些——尤其在并发量大的时候。

所以，到底你检使用哪一个呢？根据经验来看，如果是一些小型的应用或项目，那么MyISAM 也许会更适合。当然，在大型的环境下使用MyISAM 也会有很大成功的时候，但却不总是这样的。

如果你正在计划使用一个超大数据量的项目，而且需要事务处理或外键支持，那么你真的应该直接使用InnoDB方式。但需要记住InnoDB 的表需要更多的内存和存储，转换100GB 的MyISAM 表到InnoDB 表可能会让你有非常坏的体验。

字符集  GBK  utf8
===============

服务器、数据库、数据表、字段 都可以设定字符集

```
#显示支持的字符集
show character set;
#查看当前连接的字符集设置
show variables like 'character_set%';
```

数据库连接时的字符集设置
-----------

character_set_connection
:	该项设置与数据写入有关：必须与数据库中的数据字符集相一致，否则写入的内容存入数据库以后是乱码。默认为：latin1

character_set_results
:	该项设置与数据的显示显示有关：必须与输出窗口的字符集（网页的编码，命令行的字符集）相一致，否则读取的内容没问题，但显示乱码。

character_set_client
:	设置为binary可以提高安全性，降低sql注入风险。

{ver}


```
#统一设置（client connection results）三项的字符集
SET names utf8;

#单独设置一个变量的字符集
SET character_set_results = utf8;
```

在使用mysqli扩展时，应使用mysqli_set_charset设置字符集，因为它内部除了执行set names以外，还会为mysql_real_escape_string指定一个适合该字符集的c库，以解决宽字节字符串的转义风险。

```
<?php
    $db = mysql_connect('localhost:3737', 'root' ,'123456');
    mysql_select_db("test");
    $a = "\x91\x5c";//"慭"的gbk编码, 低字节为5c, 也就是ascii中的"\"
 
    var_dump(addslashes($a));
    var_dump(mysql_real_escape_string($a, $db));
 
    mysql_query("set names gbk");
    var_dump(mysql_real_escape_string($a, $db));
 
    mysql_set_charset("gbk");
    var_dump(mysql_real_escape_string($a, $db));
?>
/*
$ php -f 5c.php
string(3) "慭\"
string(3) "慭\"
string(3) "慭\"
string(2) "慭"
*/
```

database字符集设置
--------------

character_set_database 该项由数据库创建语句定义，使用数据库修改语句更改。默认为该数据库创建新表时使用此字符集。

```
# 创建数据库时设定数据库字符集和校对字符集 default 关键字是为提高其他数据库的兼容性，非必须。
CREATE DATABASE db_name DEFAULT CHARACTER SET  utf8 COLLATE utf8_general_ci;

# 修改数据库的字符集，省略数据库的名字则使用当前正在使用的数据库。
ALTER DATABASE db_name DEFAULT CHARACTER SET  utf8 COLLATE utf8_general_ci;
```

{b:提示：使用UTF8时为了节省空间，使用VARCHAR而不要用CHAR。否则，MySQL必须为一个CHAR(10) CHARACTER SET utf8列预备30个字节，因为这是可能的最大长度。}


数据表、列的字符集修改
------------

- 重设数据表的字符集(但不影响已有数据)

	`ALTER TABLE users CHARACTER SET utf8;`

- 如果你要改变字段的字符集，可以直接使用字段重定义语句

	`Alter Table tablename CHANGE  col_name col_name CHARACTER SET utf8;`
	这样便会改变列的字符集，同时对列中的数据进行字符集转换

- 如果要修改表的字符集，使用

	`Alter TAble tablename CONVERT TO CHARACTER SET utf8;`
	这样会改变**表和所有列**的默认字符集，并对所有数据执行转换。

- 如果你的列的字符集设置与数据的字符集不一致，直接使用Change子句会发生数据转码，本来正确的数据会转成乱码。所以应该先转成二进制列，然后再转回来。

	```
	#如果您有一个varchar列使用字符集（如latin1），但是存储的值实际上使用了其它的字符集（如utf8)
	ALTER TABLE t1 CHANGE c1 c1 BLOB;#把c1列转换为blob列
	ALTER TABLE t1 CHANGE c1 c1 varchar(255) CHARACTER SET utf8; #再把这个列转回会utf8
	```
