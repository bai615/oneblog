表单
==============================================================

|标签          | 类               |说明      |
|------------------------------------------|
|form |`.form-search`    | 表单两头为半园形 |
|form |`.form-inline`    | 表单的input元素为inline-block|
|form |`.form-horizontal`| 放置div.control-group |
|div  |`.control-group`  |块元素,用来放置表单元素|
|div  |`.controls`       |`margin-left: 180px;`|
|label|`.control-label`  |`float:left`|

<form class="form-horizontal">
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
      <input type="password" id="inputPassword" placeholder="Password">
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <label class="checkbox">
        <input type="checkbox"> Remember me
      </label>
      <button type="submit" class="btn">Sign in</button>
    </div>
  </div>
</form>

```html
<form class="form-horizontal">
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
      <input type="password" id="inputPassword" placeholder="Password">
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <label class="checkbox">
        <input type="checkbox"> Remember me
      </label>
      <button type="submit" class="btn">Sign in</button>
    </div>
  </div>
</form>
```

## 文本输入框

|属性|元素|元素|
|----------------------------|--------------------------|---------------------|
|placeholder='提示文本'|input>span.help-inline|input>span.help-block|

#### 框内提示文字

<input type="text" placeholder="提示文字">

```html
<input type="text" placeholder="提示文字">
```

#### 外部帮助信息

<form class="form-inline">
    <input type="text"> <span class="help-inline">行内帮助信息</span>
</form>
<form>
    <input type="text"><span class="help-block">块级帮助信息</span>
</form>

```html
<form class="form-inline">
    <input type="text"> <span class="help-inline">行内帮助信息</span>
</form>
<form>
    <input type="text"><span class="help-block">块级帮助信息</span>
</form>
```

## 单选 多选框

|@|@|@|@|
|------------------|------------|----------|------------|
|**label** 类 |`.checkbox`    |`.radio` |`.inline`|
|用法|{col:3}`div > label.checkbox > input:checkbox` <br/>`div > label.radio > input:radio`|

<label class="checkbox"> <input type="checkbox" value="">复选 </label>
<label class="radio"> <input type="radio" name="optionsRadios" id="optionsRadios1" value="yes">是 </label>
<label class="radio"> <input type="radio" name="optionsRadios" id="optionsRadios2" value="no">否 </label>

```html
<label class="checkbox">
  <input type="checkbox" value="">复选
</label>
<label class="radio">
  <input type="radio" name="optionsRadios" value="yes">是
</label>
<label class="radio">
  <input type="radio" name="optionsRadios" value="no">否
</label>
```

<div>
    <label class="checkbox inline">
         <input type="checkbox" value="">选项一
    </label>
    <label class="checkbox inline">
         <input type="checkbox" value="">选项二
    </label>
</div>
<div>
    <label class="radio inline">
         <input type="radio" name="optionsRadios" value="yes">是
    </label>
    <label class="radio inline">
         <input type="radio" name="optionsRadios" value="no">否
    </label>
</div>

```html
<div>
    <label class="checkbox inline">
         <input type="checkbox" value="">选项一
    </label>
    <label class="checkbox inline">
         <input type="checkbox" value="">选项二
    </label>
</div>
<div>
    <label class="radio inline">
         <input type="radio" name="optionsRadios" value="yes">是
    </label>
    <label class="radio inline">
         <input type="radio" name="optionsRadios" value="no">否
    </label>
</div>
```

## Prepend Append

|@|@|@|@|
|-------|------------------|--------------|---------------|
|类 |`.input-prepend"`    |`.input-append` |`.add-on`|
|用法|{col:3}`div.input-prepend > span.add-on + input:text` <br>`div.input-append  >  input:text + span.add-on` <br>`div.input-append      >  input:text + button.btn`|

<div class="input-prepend">
  <span class="add-on">@</span>
  <input class="span2" id="prependedInput" type="text" placeholder="Username">
</div>
<div class="input-append">
  <input class="span2" id="appendedInput" type="text">
  <div class="add-on">.00</div>
</div>
<div class="input-append input-prepend" style="margin-left:1px;">
  <span class="add-on">$</span>
  <input class="span2" id="appendedPrependedInput" type="text">
  <span class="add-on">.00</span>
</div>

```html
<div class="input-prepend">
  <span class="add-on">@</span>
  <input class="span2" id="prependedInput" type="text" placeholder="Username">
</div>
<div class="input-append">
  <input class="span2" id="appendedInput" type="text">
  <span class="add-on">.00</span>
</div>
<div class="input-append input-prepend">
  <span class="add-on">$</span>
  <input class="span2" id="appendedPrependedInput" type="text">
  <span class="add-on">.00</span>
</div>
```

<div class="input-append">
  <input class="span2" id="appendedInputButtons" type="text">
  <button class="btn" type="button">Search</button>
  <button class="btn" type="button">Options</button>
</div>

```html
<div class="input-append">
  <input class="span2" id="appendedInputButtons" type="text">
  <button class="btn" type="button">Search</button>
  <button class="btn" type="button">Options</button>
</div>
```

#### 前后都可以附加下拉菜单按钮

<div class="input-append">
  <input class="span2" id="appendedDropdownButton" type="text">
  <div class="btn-group">
    <button class="btn dropdown-toggle" data-toggle="dropdown">
      Action
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" style="padding:0;margin:0">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>
     </ul>
  </div>
</div>

```html
<div class="input-append">
  <input class="span2" id="appendedDropdownButton" type="text">
  <div class="btn-group">
    <button class="btn dropdown-toggle" data-toggle="dropdown">
      Action
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#">Action</a></li>
      ...
    </ul>
  </div>
</div>
```

<form class="form-search">
  <div class="input-append">
    <input type="text" class="span2 search-query">
    <button type="submit" class="btn">Search</button>
  </div>
</form>

```html
<form class="form-search">
  <div class="input-append">
    <input type="text" class="span2 search-query">
    <button type="submit" class="btn">Search</button>
  </div>
</form>
```
## 尺寸控制

>可以对文本输入框和下拉选择框使用 `.span1 ~ .span12`进行尺寸控制,还可以
>用**`div.controls-row`**容纳`input.span*`,自动为相邻的input设置间距

<form class="bs-docs-example" style="padding-bottom: 15px;">
            <div class="controls">
              <input class="span5" type="text" placeholder=".span5">
            </div>
            <div class="controls controls-row">
              <input class="span4" type="text" placeholder=".span4">
              <input class="span1" type="text" placeholder=".span1">
            </div>
            <div class="controls controls-row">
              <select class="span3">
                <option>span3</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
              <input class="span2" type="text" placeholder=".span2">
            </div>
            <div class="controls controls-row">
              <input class="span2" type="text" placeholder=".span2">
              <select class="span3">
                <option>span3</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
            <div class="controls controls-row">
              <input class="span1" type="text" placeholder=".span1">
              <input class="span4" type="text" placeholder=".span4">
            </div>
          </form>

```html
<form class="bs-docs-example" style="padding-bottom: 15px;">
            <div class="controls">
              <input class="span5" type="text" placeholder=".span5">
            </div>
            <div class="controls controls-row">
              <input class="span4" type="text" placeholder=".span4">
              <input class="span1" type="text" placeholder=".span1">
            </div>
            <div class="controls controls-row">
              <input class="span3" type="text" placeholder=".span3">
              <input class="span2" type="text" placeholder=".span2">
            </div>
            <div class="controls controls-row">
              <input class="span2" type="text" placeholder=".span2">
              <input class="span3" type="text" placeholder=".span3">
            </div>
            <div class="controls controls-row">
              <input class="span1" type="text" placeholder=".span1">
              <input class="span4" type="text" placeholder=".span4">
            </div>
          </form>
```

## Form提交按钮

<div class="form-actions">
  <button type="submit" class="btn btn-primary">Save changes</button>
  <button type="button" class="btn">Cancel</button>
</div>

```html
<div class="form-actions">
  <button type="submit" class="btn btn-primary">Save changes</button>
  <button type="button" class="btn">Cancel</button>
</div>
```
