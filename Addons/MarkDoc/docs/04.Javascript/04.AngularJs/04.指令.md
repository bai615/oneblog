指令
=====

> AngularJS内建有一套完整的、可扩展的、用来帮助web应用开发的指令集，它使得HTML可以转变成“特定领域语言(DSL)”

从HTML中调用指令
------------

> 指令遵循驼峰式命名，如`ngBind`。指令可以通过使用指定符号转化成snake case的名称来调用，特定符号包括 `: - _` 。
你还可以选择给指令加上前缀，比如`x-`，`data-`来让它符合html的验证规则。

> 例如：`ng:bind, ng-bind, ng_bind, x-ng-bind , data-ng-bind`等效于调用`ngBind指令`

> 指令可以做为元素名，属性名，类名，或者注释。不过大多数情况指令作为元素的属性使用


指令创建示例
--------

> 你可以创建自定义的指令，用工厂方法返回一个函数作为指令的执行逻辑。示例：


```
angular.module('time', []).directive('myCurrentTime', //指令命名为myCurrentTime 
	function($timeout, dateFilter) { //为指令工厂注入myCurrentTime指令将要依赖的$timeout和dateFilter组件
						//指令所在范围，指令所在元素，指令所在元素的属性对象
		return function(scope, element, attrs) {  //返回指令函数
			var format,timeoutId; 
		
			function updateTime() {
				element.text(dateFilter(new Date(), format));
			}
		
			scope.$watch(attrs.myCurrentTime, function(value) {
				format = value;
				updateTime();
			});
		
			function updateLater() {
				timeoutId = $timeout(function() {
					updateTime();
					updateLater();
				}, 1000);
			}
		
			element.bind('$destroy', function() {
				$timeout.cancel(timeoutId);
			});
		
			updateLater();
		}
  });

//控制器
function Ctrl2($scope) {
  $scope.format = 'M/d/yy h:mm:ss a';
}
```

视图：指令的值可以直接使用模型名称

```
<!doctype html>
<html ng-app="time">
  <head>
    <script src="http://code.angularjs.org/1.0.6/angular.min.js"></script>
    <script src="script.js"></script>
  </head>
  <body>
    <div ng-controller="Ctrl2">
      Date format: <input ng-model="format"> <hr/>
      Current time is: <span my-current-time="format"></span>
    </div>
  </body>
</html>
```

创建指令完整方式
---------

> 定义factory(injectables)函数，返回一个 **指令定义对象** ,对象的成员见以下

```
angular.module('time',[]).directive('directiveName', function factory(injectables) {
  var directiveDefinitionObject = {
    name       : '',               // 可选，当前作用域的名称
    priority   : 0,                //优先级,当一个dom上有多个指令时，需要用优先级确定指令的执行顺序
    terminal   : false,            // If set to true then the current priority will be the last set of directives which will execute (any directives at the current priority will still execute as the order of execution on same priority is undefined).
    template   : '<div></div>',
    templateUrl: 'directive.html',
    replace    : false,
    transclude : false,
    restrict   : 'A',
    scope      : false, // 
    controller : function($scope, $element, $attrs, $transclude, otherInjectables) { ... },
    compile    : function compile(tElement, tAttrs, transclude) {
					return {
						pre: function preLink(scope, iElement, iAttrs, controller) { ... },
						post: function postLink(scope, iElement, iAttrs, controller) { ... }
					}
    },
    link       : function postLink(scope, iElement, iAttrs) { ... }
  };
  return directiveDefinitionObject;
});
```

大多情况无需每个属性都定义，

```
angular.module('time',[]).directive('directiveName', function factory(injectables) {
  var directiveDefinitionObject = {
    compile    : function compile(tElement, tAttrs) {
						return function postLink(scope, iElement, iAttrs) { ... }
    },
  };
  return directiveDefinitionObject;
});
```

甚至更简化为前面示例的方式或下面的方式，

```
angular.module('time',[]).directive('directiveName', function factory(injectables) {
	return function postLink(scope, iElement, iAttrs) { ... }
});
```

指令定义对象
------------------




作用域
===========

模块
=====
