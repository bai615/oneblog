window.document
=================

它是浏览器对DOM core 中Document对象的扩展HTMLDocument，以方便地操作html文件。

主要扩展内容为：增加了一个获取元素的方法，根据html定义，由Element对象扩展出HTMLElement对象并增加了相应的成员，以更加方便地获取html元素和属性;

扩展结构为：Element <- HTMLElment <- 各类特殊HTML元素对象(HTMLImageElement等) <- html标签对象(Image对象等)

获取元素
-----------

> 下面方式获取到的元素对象，都是HTMLELment对象的一种对应标签的扩展对象，例如，获取的元素是a标签元素，则得到的是HTMLLinkElement对象

### 方式一

* getElementByID()
* getElementByTagName()
* getElementByName()

### 方式二

> 浏览器会为许多标签在Doucment对象下创建专门的DOM元素数组集合，
> 数组含有文档中所有该标签的DOM元素,对于支持name属性的标签(form,iframe)，还可以通过name访问数组元素

> 通过以下方式得到的元素对象，都是HTMLElement对象的扩展：HTMLIFrameElement,HTMLLinkElement,....

* document.forms
* document.embeds
* document.scripts
* document.images 
* document.links 
* document.styleSheets
* document.anchors
* document.frames
* document.applets

可以用下面属性获取文档的所有节点或者body节点：

* document.all
* document.body

获取和设置文档信息
-----------

* document.title
* document.URL
* document.domain
* document.referrer
* document.cookie


### 关于Cookie

cookie与query string类似，只是一串分号分隔的key=value字符串： cname=value;expires=GMTstring;path=thepath;domain=domain_name;secure

读取时， document.cookie返回的是整个字符串，包含了所有的cookie信息

而设置时,每次只能设置一对key/value：

```
document.cookie="cookiename1=value1";  //赋第一对值添加到浏览器,每次只能赋一对
document.cookie="cookiename2=value2";//赋第2对值添加到浏览器(因为cookie保存在客户端，所以不会覆盖第一对值，除非cooke名重复)
```


HTMLElement
==============

> HTMLElement对象是Html文档的元素的基本对象，为所有html元素提供了通用的成员。
> HTMLElement扩展自DOM Core的Element对象，所以也拥有Element对象的所有成员.
> 由于HTMLElement是由浏览器实现，所以不同的浏览器的实现上也有所不同，即所谓兼容性问题。
> 大多情况下，我们通过使用js框架来操作html的元素对象，以避免兼容性问题。

> 下面只介绍一些常见的，通用的成员

|通用属性    |说明                                                                                                     |
|------------|---------------------------------------------------------------------------------------------------------|
|children    |子元素数组，数组中的元素是HTMLElement对象的扩展元素对象，可以使用元素自身特有的成员属性                  |
|innerHTML   |元素内部的内容的html代码                                                                                 |
|outerHTML   |元素及元素内部的内容的html代码                                                                           |
|scrollLeft  |可写, 元素横向滚动距离(定宽div出现滚动条时)                                                                    |
|scrollTop   |可写, 元素纵向滚动距离(定高div出现滚动条时)                                                                    |
|scrollHeight|只读, 元素出现滚动条以后，内容实际上占用的高度，即padding+内容的高度(包括在可视区域外的的部分)，也即从一端滚动到另一端所能看到的滚动条以内的内容的总高宽|
|scrollWidth |只读, 元素出现滚动条以后，内容实际上占用的宽度，即padding+内容的宽度(包括在可视区域外的的部分)，也即从一端滚动到另一端所能看到的滚动条以内的内容的总宽度|
|offsetParent|只读, DOM元素的offsetTop/Left的参考元素（默认为body），返回HTMLElement对象                                     |
|offsetLeft  |只读, DOM元素距离文档左边的距离                                                                                |
|offsetTop   |只读, DOM元素距离文档顶端的距离                                                                                |
|offsetHeight|只读, 元素的的总盒模型高度（内容+滚动条+padding+border+被父容器遮挡的部分）                                    |
|offsetWidth |只读, 元素的的总盒模型宽度（内容+滚动条+padding+border+被父容器遮挡的部分）                                    |
|clientLeft  |只读, 元border-left的宽度|
|clientTop   |只读, 元border-top的高度|
|clientHeight|只读, 元元素自身的可视内容区域高度(包括padding，不包括border、margin；不包括滚动条的宽度)                  |
|clientWidth |只读, 元元素自身的可视内容区域宽度(包括padding，不包括border、margin；不包括滚动条的宽度)                  |
|style       |返回一个CSSStyleDeclaration JSON对象，可以读取或设置任意的css属性,例如：`element.style.paddingTop="10px"`|
|className   |返回class属性的值                                                                                        |
|dir         |-                                                                                                        |
|lang        |-                                                                                                        |
|title       |-                                                                                                        |
|id          |-                                                                                                        |


#### 需要注意的是：对于body元素

* document.body.clientHeight  是指文档实际高度
* document.body.clientWidth  是指文档实际宽度

扩展html元素对象
===============

任何html规范定义的标签，都有自己对应的HTMLElement扩展，都可以通过元素对象，直接访问和设置
元素自身特有标签属性。

以 a 标签为例，你可以直接访问和设置a标签的href,target等属性，因为这些属性属于html标准DOM规范。
如果你设置一个在html规范中不存在的属性，它并不会作为html标签的DOM属性对待，而仅仅是被当做对象的一个自定义对象成员对待。

例如：

```
alink.href='newurl';//设置后，alink元素DOM的href属性立即更新为newurl
alink.data='userdata';//设置后，alink元素DOM并不会增加一个data属性,data仅仅是alink对象的一个对象成员
```

Image对象
---------


