绑定事件
==========

> 绑定事件传入的data参数，在回调函数中通过eventObject.data调用

方式1:事件方法
------------

> 用来为元素绑定事件和触发事件,不传参表示触发事件

> 参数示例：`.blur( [eventData ,] handler(eventObject) )` 绑定事件函数,第一个可选参数是传入事件函数的数据

{r:注意：只能用在html源文件的元素上。如果元素是js后来动态加入的，不会有效。}

|事件方法     |说明                                                                                                                                                                  |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|.blur()      |失去焦点，通常只对表单元素有效，现代浏览器已扩展到所有元素                                                                                                            |
|.change()    |内容改变,只对`<input>  <textarea>  <select>` 有效                                                                                                                     |
|.click()     |点击事件：鼠标从移动到元素上，按下键，释放键                                                                                                                          |
|.dblclick()  |双击事件:两次单击事件                                                                                                                                                 |
|.error()     |浏览器载入外部文件(例如：图片，音乐，flash,js,css)失败                                                                                                                |
|.focus()     |获得焦点                                                                                                                                                              |
|.focusin()   |表示元素内部的子元素获得焦点                                                                                                                                          |
|.focusout()  |表示元素内部的子元素失去焦点                                                                                                                                          |
|.keydown()   |只有元素获得焦点后，才能触发事件。（按键按下（是keypress之前的时间）。只识别键位，不管键位上对应的是哪个符号）                                                        |
|.keypress()  |只有元素获得焦点后，才能触发事件。（按键处于被按下状态（keydown事件之后的事件）。可识别大小写，shift的上下字符,不支持Alt，ctrl,shift,win,context以及功能键区的功能键）|
|.keyup()     |只有元素获得焦点后，才能触发事件。按键释放                                                                                                                            |
|.load()      |标识一个元素及其内部的所有内容都完全载入(包括引入的外部文件),可用于img,frame,script,window对象                                                                        |
|.mousedown() |鼠标按键按下，不区分左右中键。按下并保持，不会重复触发。                                                                                                              |
|.mouseup()   |鼠标按键松开，不区分左右中键。                                                                                                                                        |
|.mouseenter()|(仅当鼠标移入选定的元素自身时触发，进入子元素后不会再次触发),jquery事件                                                                                                                                              |
|.mouseleave()|(仅当鼠标离开选定的元素自身后触发，离开子元素后不会再次触发),jquery事件                                                                                                                                    |
|.mousemove() |鼠标在元素上面移动,js事件                                                                                                                                               |
|.mouseout()  |(指针离开元素或元素的子元素时都会触发),js事件                                                                                                                                          |
|.mouseover() |(鼠标移入元素或元素的子元素时都会触发),js事件                                                                                                                                    |
|.resize()    |只能用于$(window)|
|.scroll()    |可以用于Window,frame,溢出的div等任何有滚动条的元素|
|.select()    |仅用于`<input type="text"> <textarea>`,当用鼠标选择文本完成后触发 |
|.submit()    |仅用于`<form>`|
|.unload()    |(离开页面（关闭标签，关闭浏览器，转到其他页面）)                                                                                                                      |
|.hover()     |`.hover( handlerIn(eventObject),handlerOut(eventObject))`悬停事件，绑定两个函数                                                                                       |



#####示例：

```javascript
$("#element").click(function(){
        statements;
});//绑定事件
$('#element').click();//触发事件
```

### 禁止事件默认行为

```javascript
$('#menu').click(function(evt){
    evt.preventDefault();
    statements;
});

$('#menu').click(function(){
    statements;
    return false;
});
```


### 阻止事件冒泡

```javascript
$('#theLink').click(function(evt) { 
            statements //如果祖先元素也绑定有click事件，这部分内容也会执行 
            evt.stopPropagation(); //  阻止冒泡，继续向下执行
            continuing //从这里开始的内容只对被点击的元素有效
});

```


方式2：`.bind()`
------------

> 通过data对象，使得事件的处理函数更灵活 

```javascript
var linkVar = { message:'Hello from a link'}; //定义一个数据对象
var pVar = { message:'Hello from a paragraph'}; //定义另一个数据对象
 
function showMessage(evt) { 
        alert(evt.data.message);
}

//同一个事件处理函数可用于不同的元素，通过data参数，控制具体执行行为
$('a').bind('click',linkVar,showMessage); 
$('p').bind('mouseover',pVar,showMessage);
```

### 一次绑定多个事件

#### 多个事件绑定同一个回调函数

多个事件以空格分割传入第一个参数

```javascript
$(document).bind('click  keypress', function() { 
            $('#lightbox').hide( );
 }); // end bind
```

#### 使用json对象一次绑定多个不同的事件

```javascript
$('#theElement').bind({ 
        'click' 	: function() { },
        'mouseover' : function() { };
}); // end bind
```


方法3：on()
----------------

> `.on( events [, selector] [, data], handler(eventObject) )`

> `.on( events-map [, selector] [, data] )`


```javascript
$("#dataTable tbody").on("click", "tr", function(event){
          alert($(this).text());
});
```

### 事件命名空间

事件名称可以限定命名空间,以便更加灵活地触发和移除.例如:"click.myPlugin.simple"
为click事件定义了myPlugin和simple两个命名空间.你可以通过.off("click.myPlugin") 或 .off("click.simple") 
移除一个命名空间下的事件而不影响另一个命名空间下的事件.(下划线开头的命名空间是jquery的保留命名空间)

### on()禁止事件默认行为

```javascript
//handler(eventObject)参数如果使用false可以禁用事件的默认行为
$("form").on("submit", false)
```

### on()绑定多个事件


示例：

```javascript
$("div.test").on({
          click: function(){
                $(this).toggleClass("active");
          },
          mouseenter: function(){
                $(this).addClass("inside");
          },
          mouseleave: function(){
                $(this).removeClass("inside");
          }
)};
```


方法4：`.one()`
------

> 参数和用法与on()相同，它为元素绑定一个一次性事件，在被触发过一次后自动移除

.one( events [, selector ] [, data ], handler(eventObject) )


 
移除事件
============


```javascript
$(".element").unbind("click");
$(".element").unbind();//移除该元素上的所有事件

```


{g:.off() 只用来移除 .on()绑定的事件}



触发事件
======

`.trigger(type[,data])`
-------------------

type
: 一个事件对象或者要触发的事件类型

data
: 传递给事件处理函数的附加参数(用于已绑定事件的元素)


`.triggerHandler(type[,data])`
---------------------

##### 这个方法的行为表现与trigger类似，但有以下三个主要区别：

1. 他不会触发浏览器默认事件,也不会产生冒泡。

1. 只触发jQuery对象集合中第一个元素的事件处理函数。而trigger()方法触发结果集所有元素上的事件

1. 这个方法的{b:返回的是事件处理函数的返回值}，而不是据有可链性的jQuery对象。此外，如果最开始的jQuery对象集合为空，则这个方法返回 undefined 。


eventObject
============


> jquery事件对象拥有原生事件对象的属性和一些跨浏览器的新属性和方法

|事件对象属性  |说明                                                                                              |
|--------------|--------------------------------------------------------------------------------------------------|
|pageX         |鼠标离document左边缘的距离                                                                        |
|pageY         |鼠标离document上边缘的距离                                                                        |
|screenX       |鼠标离显示屏左边缘的距离                                                                          |
|screenY       |鼠标离显示屏上边缘的距离                                                                          |
|clientX       |相对于浏览器可视区域左边缘                                                                        |
|clientY       |相对先于浏览器可视区域上边缘                                                                      |
|data          |绑定事件是在参数中传入的数据对象：bind(type,[data],fn)中的data                                    |
|shiftKey      |如果事件发生时shift键是按下的，该值为true                                                         |
|which         |keypress事件发生的keyCode码，可以使用标准String.fromCharCode(evt.which)将keyCode转为实际对应的字符|
|target        |DOM元素对象:事件是从哪个元素上被唤起（例如：嵌套div最外层绑定了click事件，点击内层div会触发该事件，内层的div就是target元素）|
|currentTarget |DOM元素对象:事件回调函数现在正在作用于哪个元素上(冒泡情况)，在没有使用namespace的情况下，该属性等效于this关键字                                               |
|delegateTarget|事件时在哪个元素上绑定的,也就是调用on()方法的那个jqeruy元素对象                                                                                                 |
|relatedTarget |相关的DOM元素对象。例如：mouseover事件的相关对象就是鼠标在进入目标元素之前的那个元素。                                                                                                 |
|result        |元素绑定的事件上一次被触发时事件处理函数的返回内容(例如事件每次触发都返回一个值，该属性记录了最近的一个值)|
|timeStamp     |事件发生时的毫秒时间戳                                                                            |
|type          |事件的类型                                                                                        |
|altKey        |事件发生时Alt是否被按下                                                                           |
|ctrlKey       |事件发生时ctrl是否被按下                                                                          |
|shiftKey      |事件发生时shift是否被按下                                                                         |
|metaKey|记录事件发生时metakey是否被按下|
|which|对于按键和鼠标事件，记录了被按下的键的标识|

|事件对象方法                   |说明                                                    |
|-------------------------------|--------------------------------------------------------|
|stopImmediatePropagation()     |剩余的部分不再执行，并阻止冒泡                          |
|stopPropagation()|阻止冒泡|
|preventDefault()|阻止默认事件 |
|isDefaultPrevented()           |是否已经执行过preventDefault()方法，返回布尔值          |
|isImmediatePropagationStopped()|是否已经执行过stopImmediatePropagation()方法，返回布尔值|
|isPropagationStopped()         |是否已经执行过stopPropagation() 方法，返回布尔值        |


共同特征
=========

> 都返回$(this),所以都可以连续调用（triggerHandler除外）
