特效
=========

> 特效的执行与ajax一样,也是异步执行.动画的位于队列中

####特效执行过程:

1. 检查自己是否已存在于队列中
2. 如果不存在，将自己放入队列(只放入一次)
3. 执行下一条js语句


####队列的执行过程：

1. 队列中的动画依次执行
1. 可使用stop()方法立即停止正在执行的动画,并进入下一个动画；
1. 可以使用.dequeue()方法在不停止正在执行的动画的情况下立即开始执行下一个动画。
1. 队列在执行过程中可以使用.queue( [queueName], newQueue )切换元素绑定的队列


内置jQuery Element Object特效方法
---------------------------------

####内置特效一般方法有三个参数：[speed,[easing],[fn]]

speed
: 用来设定效果执行的速度，可以使用毫秒数，也可以使用slow,fast,normal
	
easing
: 指定切换效果。默认为swing，可选linear。更多效果(附件)：http://gsgd.co.uk/sandbox/jquery/easing/
: 各种运动效果的数学曲线图 http://www.robertpenner.com/easing/easing_demo.html 

fn
: 指定动画执行完以后执行的回调函数

|方法                                     |说明                                      |
|-----------------------------------------|------------------------------------------|
|hide()                                   |隐藏                                      |
|show()                                   |显示                                      |
|toggle()                                 |显藏切换                                  |
|fadeIn()                                 |淡入                                      |
|fadeOut()                                |淡出                                      |
|fadeToggle()                             |淡入淡出切换                              |
|fadeTo ([[speed],opacity,[easing],[fn]]) |渐进式调整透明度到指定值                  |
|slidUp()                                 |收起                                      |
|slidDown()                               |放下                                      |
|slideToggle()                            |收起放下切换                              |


特效控制
-------------


<p class="api">delay(duration,[queueName])</p>

延迟队列

duration
: 延迟的时间;

queueName
: 延迟的队列,默认是当前动画队列

<p class="api">queue([queueName])</p>

返回queueName队列中将要执行的函数组成的数组（默认为fx）

<p class="api">queue([queueName],callback())</p>

手动向队列末端添加回调函数,它会在队列(默认fx)正在执行的动画执行结束后被调用.

callback(next)：添加到队列中的新函数(函数内含有非动画的语句)

返回值为jQuery对象

回调函数的最后一句通常是:$(this).dequeue();

```
$('#foo').slideUp();
$('#foo').queue(function() {
  alert('Animation complete.');
  $(this).dequeue();
});
```

<p class="api">queue([queueName],newQueue)</p>

切换队列，切换后执行新队列，原队列正在执行中的动画会正常执行到结束。

queueName
: 默认为fx,原队列

newQueue
: 要执行的新队列,它是一个函数组成的数组

<p class="api">dequeue([queueName])</p>

立即执行(不论在正在执行的动画是否已经完成)队列中的下一个还未开始的动画
 
<p class="api">clearQueue([queueName])</p>

从队列中删除所有还未运行的项目
 
<p class="api">stop([clearQueue][,jumpToEnd])</p>
<p class="api">stop([queue][,clearQueue][,jumpToEnd])</p>

停止正在执行的动画的执行

clearQueue
: 布尔值，是否清空队列内的动画,为true的话,后面的动画都将不会执行,为否的话后面的动画依旧会执行。

jumpToEnd
: 布尔值，是否在停止当前动画之后将元素的状态设置为当前动画正常结束后的状态；

<p class="api">finish([queue])</p>

1.9新增

当调用该方法时,元素上正在运行的动画将立即停止,队列之后的所有动画也不再执行,元素的状态设置为所有动画正常结束后的状态.可以理解为,所有动画0秒内执行完成.


自定义动画
--------------

> 通过控制元素css的numeric类型的属性变化实现动画效果

<p class="api">animate(params,options)</p>

* params(json对象)

	css属性作为key,终值作为value
	
* options(json对象)	

		{
			duration:      //(default: 400)动画运行时间
			easing:        //(default: swing)swing,linner
			queue:         //(default: true)布尔值:为真表示动画放入队列，为假表示马上执行;也可以是一个字符串队列名,自定义动画被添加到这个队列中
			specialEasing: //为每一个css属性单独指定切换效果
			step:          //<span class="api">Function( Number now, fx )</span>每一步结束后执行的回调函数
			progress:      //<span class="api">Function( Promise animation, Number progress, Number remainingMs )</span>1.8
			complete:      //动画结束后运行的回调函数
			done:          // <span class="api">Function( Promise animation, Boolean jumpedToEnd )</span> 1.8
			fail:          // <span class="api">Function( Promise animation, Boolean jumpedToEnd )</span> 1.8
			always:        // <span class="api">Function( Promise animation, Boolean jumpedToEnd )</span> 1.8
		}

	- setp 

		now:是一个数值，设定step的步长,css属性每执行完这个步长的变化,就会执行一次这个回调函数
		fx:是jQuery.fx对象的引用，有elem(执行动画的元素DOM对象),start(初始值),end(终值),prop(参与动画过程的css属性)等只读的属性
		
		
		
		
		
		
		
		
		
		