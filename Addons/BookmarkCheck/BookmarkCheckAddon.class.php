<?php

namespace Addons\BookmarkCheck;
use Common\Controller\Addon;

/**
 * 书签检查器插件
 * @author yangweijie
 */

    class BookmarkCheckAddon extends Addon{

        public $info = array(
            'name'=>'BookmarkCheck',
            'title'=>'书签检查器',
            'description'=>'用于检查一个html中指定规则全部链接',
            'status'=>1,
            'author'=>'yangweijie',
            'version'=>'0.1'
        );

        public function install(){
            return true;
        }

        public function uninstall(){
            return true;
        }

        //实现的sidebar钩子方法
        public function single($param){
            if ($param['name'] == 'BookmarkCheck') {
                $this->display('single');
            }
        }

    }