# ThinkSDK  SNS通用开发者插件

## 用途

给ONETHINK集成SNS登录后可以方便 调用第三方接口。

## 提供了

* ThinkSDK类库
* qq和sina的登录和调用接口实例
* 一个oauth表和后台列表方便以后处理继续授权和用户绑定查看的事
* 独立的thinksdk接口 提供 login（登录）、call（调用）、check（检查）三个方法（够用了）
* 配置ak、sk和meta验证和单独控制2个登录是否启用
* 一个pageheader方便嵌入登录资源链接

## 使用流程
1. 安装好插件
2. 配置插件里的meta 通过相关平台的应用审核 
3. 填写配置里的ak sk 添加测试账号 ![配置](http://ww2.sinaimg.cn/mw1024/50075709jw1ei5fptn506j20la0kmaaz.jpg)
4. 用 `{:hook('thinksdk', array('action'=>'login'))}` 添加登录框` 效果：![登录处钩子](http://ww2.sinaimg.cn/mw1024/50075709jw1ei5fpue70dj20ah07f3yo.jpg)
5. 点击登录后，检查入库和显示 ![sina登录并获取用户信息](http://ww3.sinaimg.cn/mw1024/50075709jw1ei5fpv1amwj21370evq65.jpg) ![qq登录获取信息](http://ww1.sinaimg.cn/mw1024/50075709jw1ei5fpvq4udj212d0ertcs.jpg)
6. 要调用sns接口的地方先用

    $param = array('action'=>'check','type'=>'qq')
    `\Think\Hook::listen('thinksdk', $param);` //获取token $param 是引用的，因此通过$param 是否是false来判断失败，返回数组是该type的token信息表数据。

    然后 再通过这个token信息，调用接口 如
    $param2 = array(
        action'=>'call',
        'token'=>$param,
        'api'=>'users/show',
        'type'=>'sina',
    );
    \Think\Hook::listen('thinksdk', $param2);

7. call 的返回值是个标准数组 `array('status'=>1,'data'=>array())``

## 注意点
* 由于框架原因，本来设计的hook函数 参数是引用的，让用户可以通过hook来修改某些中间变量，但是那样的话，调用时候这个地方的参数必须是一个变量，而不是像手写的array()了。可能由于这个原因，老大优化没有拔hook函数参数设为引用参，也没有返回值。因为`Hook::lisen`也没有返回值 所有调用call的 时候大家一定要用 `\Think\Hook::lisen` 当然不需要用引用参数的直接 hook函数好了
* 关于回调地址，由于各个提供服务的厂商为了保证访问性，回调时访问的地址统一用传统的getURL参数，因此，我们在登录调用oauth的url上也得使用get参数 但是这样的话要限制url_model参数为原始的兼容模式，后来研究，只需要部分参数为get就行了如：

    http://jaylabs.sinaapp.com/Home/Addons/execute?_addons=ThinkSDK&_controller=ThinkSDK&_action=callback&type=

http://jaylabs.sinaapp.com大家换成自己的域名。

* 有一些第三方登录看苗儿的代码，成功判断依赖某一个接口，就没去验证，交给大家去研究实现，本插件只是一个通用的解决方案。更多接口没时间测试也用不到。就做了2个最常用的
* sina 微博的授权，有效期返回的时间戳很诡异，拿当前的加上，变成2019年的5年后了，单独转换有不是完整的时间戳长度，因此暂时SinaSdk里手动写死了1小时后。
* 大家如果要自己用，一定要将插件中的代码TODO改为自己的逻辑。当然用我OneBlog的单用户的可以不改。
* 欢迎大家在我博客 [OneBlog](http://git.oschina.net/yangweijie/oneblog) 上提交issue和pull request。比如完成自动后台续期token

