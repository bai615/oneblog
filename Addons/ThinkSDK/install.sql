CREATE TABLE IF NOT EXISTS `onethink_oauth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) unsigned NOT NULL COMMENT '用户表id',
  `type` enum('qq','baidu','diandian','douban','github','google','kaixin','msn','qq','renreb','sina','souhu','t163','taobao','tencent','x360') NOT NULL DEFAULT 'qq' COMMENT '类型 如QQ、Baidu、',
  `name` varchar(100) DEFAULT '' COMMENT '账号昵称',
  `access_token` varchar(100) DEFAULT '' COMMENT 'ak',
  `expires_in` datetime DEFAULT NULL COMMENT '过期时间',
  `openid` varchar(50) NOT NULL DEFAULT '',
  `openkey` varchar(50) NOT NULL DEFAULT '',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1-可用 0-不可用',
  `extend` varchar(1000) DEFAULT '' COMMENT '序列化',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
