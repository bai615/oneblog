#Slog插件
本插件参考于罗飞的SocketLog，修改了其中socket部分，改为 HTML5的SSE（server send event）方案（可以理解为官方的轮询），并加了兼容性处理支持IE8+和其他主流浏览器。
解决了ThinkPHP 异步调试难的问题。

#安装
将admin.php的APP_DEBUG关闭，这样不会写调试信息的日志了，因为实时读取日志会不断访问插件中的Slog控制器的index方法，如果开着闲置时也会不间断写入无效日志信息
然后正常安装

#使用
1. 进入后台 扩展列表，找到Slog插件，进入配置页，如下图：
![打开配置](https://attachments.tower.im/tower/61042a2088854cab8e5e465c7dd98d0b?filename=Clipboard%20Image.png)
2. 配置页面设置轮询间隔时间，或者直接打开控制台，进行调试信息的观察
![](https://attachments.tower.im/tower/5b4faa5d10a047b193a0155ce601c3a7?filename=Clipboard%20Image.png)
3. 可以在调试显示页面 直接清除数据库日志表（当记录数多的时候比如10000），也可以停止轮询，当想输出调试信息前再开启（当然刷新也行）。
![](https://attachments.tower.im/tower/16c3e8f44e734b8885f210d08b45443f?filename=Clipboard%20Image.png)
4. 为了方便调试，也可以点新开窗口 只看下面的。
![](https://attachments.tower.im/tower/5b4faa5d10a047b193a0155ce601c3a7?filename=Clipboard%20Image.png)
5. slog的使用方法你就记住Slog的参数就行了，具体参见SocketLog的文档

    slog('msg','log');  //一般日志
    slog('msg','error'); //错误日志
    slog('msg','info'); //信息日志
    slog('msg','warn'); //警告日志
    slog('msg','trace');// 输入日志同时会打出调用栈
    slog('msg','alert');//将日志以alert方式弹出
    slog('msg','log','color:red;font-size:20px;');//自定义日志的样式，第三个参数为css样式

一般用 log类型，想知道具体在哪调用的堆栈用trace 调试sql 用slog('sql', 数据连接);
[SocketLog项目主页](https://github.com/luofei614/SocketLog)。


